import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {SnotifyModule, SnotifyPosition, SnotifyService} from 'ng-snotify';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { DateTimePickerModule } from "@syncfusion/ej2-angular-calendars";
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import {CoreModule} from './core/core.module';
import {RouterModule} from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { CompleteSignupComponent } from './components/complete-signup/complete-signup.component';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {EditorModule} from '@tinymce/tinymce-angular';
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import {
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {ImageCropperModule} from 'ngx-image-cropper';
import {NgxFileDropModule} from 'ngx-file-drop';
import {Ng5SliderModule} from 'ng5-slider';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import { CheckBoxModule } from '@syncfusion/ej2-angular-buttons';


import {TermsConditionsComponent} from './components/terms-conditions/terms-conditions.component';
import { BlogComponent } from './components/blog/blog.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { AboutComponent } from './components/about/about.component';
import { VisionComponent } from './components/vision/vision.component';
import { OurTeachersComponent } from './components/our-teachers/our-teachers.component';
import { FaqComponent } from './components/faq/faq.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { NewsComponent } from './components/news/news.component';
import { ActivityComponent } from './components/activity/activity.component';
import {ProfileViewComponent} from './components/teaching/teaching-component/profile/profile-view/profile-view.component';
import {AcademicsComponent} from './components/teaching/teaching-component/apply/academics/academics.component';
import {CertificateComponent} from './components/teaching/teaching-component/apply/certificate/certificate.component';
import {ExperienceComponent} from './components/teaching/teaching-component/apply/experience/experience.component';
import {IntroductionComponent} from './components/teaching/teaching-component/apply/introduction/introduction.component';
import {LandingComponent} from './components/teaching/teaching-component/apply/landing/landing.component';
import {ClassLandingComponent} from './components/teaching/teaching-component/class/class-landing/class-landing.component';
import {ClassDetailsComponent} from './components/teaching/teaching-component/class/class-details/class-details.component';
import {AdditionalInfoComponent} from './components/teaching/teaching-component/class/additional-info/additional-info.component';
import {CourseFormatComponent} from './components/teaching/teaching-component/class/course-format/course-format.component';
import {CoverImageComponent} from './components/teaching/teaching-component/class/cover-image/cover-image.component';
import {DescriptionComponent} from './components/teaching/teaching-component/class/description/description.component';
import {PaymentLandingComponent} from './components/teaching/teaching-component/payment/payment-landing/payment-landing.component';
import {PricingComponent} from './components/teaching/teaching-component/class/pricing/pricing.component';
import {ResourcesComponent} from './components/teaching/teaching-component/class/resources/resources.component';
import {WelcomePostComponent} from './components/teaching/teaching-component/class/welcome-post/welcome-post.component';
import {ScheduleLandingComponent} from './components/teaching/teaching-component/schedule/schedule-landing/schedule-landing.component';
import {PreviewClassComponent} from './components/teaching/teaching-component/preview-class/preview-class.component';
import {TopBarComponent} from './components/teaching/teaching-component/top-bar/top-bar.component';
import {SettingsComponent} from './components/teaching/teaching-component/settings/settings.component';
import {OtherProfileComponent} from './components/teaching/teaching-component/profile/other-profile/other-profile.component';
import {AvailabilityComponent} from './components/teaching/teaching-component/availability/availability.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { LearnerCompleteSignupComponent } from './components/learner-complete-signup/learner-complete-signup.component';
import { ClassSearchComponent } from './components/class-search/class-search.component';
import {CalanderModule} from "../../projects/calander/src/app/app.module";
import { FreeClassComponent } from './components/free-class/free-class.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/',  '.json');
}
export const ToastConfig = {
  global: {
    newOnTop: true,
    maxOnScreen: 8,
    maxAtPosition: 8,
    filterDuplicates: false
  },
  toast: {
    type: 'error',
    showProgressBar: false,
    timeout: 3000,
    closeOnClick: true,
    pauseOnHover: true,
    bodyMaxLength: 150,
    titleMaxLength: 16,
    backdrop: -1,
    icon: null,
    iconClass: null,
    html: null,
    position: SnotifyPosition.centerBottom,
    animation: {enter: 'fadeIn', exit: 'fadeOut', time: 400}
  },
  type: {
    prompt: {
      timeout: 0,
      closeOnClick: false,
      buttons: [
        {text: 'Ok', action: null, bold: true},
        {text: 'Cancel', action: null, bold: false},
      ],
      placeholder: 'Enter answer here...',
      type: 'prompt',
    },
    confirm: {
      timeout: 0,
      closeOnClick: false,
      color: 'black',
      buttons: [
        {text: 'Ok', action: null, bold: true},
        {text: 'Cancel', action: null, bold: false},
      ],
      type: 'confirm',
    },
    simple: {
      type: 'simple'
    },
    success: {
      type: 'success'
    },
    error: {
      showProgressBar: false,
      timeout: 2500,
      type: 'error'
    },
    warning: {
      type: 'warning'
    },
    info: {
      type: 'info'
    },
    async: {
      pauseOnHover: false,
      closeOnClick: false,
      timeout: 0,
      showProgressBar: false,
      type: 'async'
    }
  }
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    TermsConditionsComponent,
    CompleteSignupComponent,
    BlogComponent,
    ContactUsComponent,
    AboutComponent,
    VisionComponent,
    OurTeachersComponent,
    FaqComponent,
    GalleryComponent,
    NewsComponent,
    ActivityComponent,
    ProfileViewComponent,
    AcademicsComponent,
    CertificateComponent,
    ExperienceComponent,
    IntroductionComponent,
    LandingComponent,
    ClassLandingComponent,
    ClassDetailsComponent,
    AdditionalInfoComponent,
    CourseFormatComponent,
    CoverImageComponent,
    DescriptionComponent,
    PaymentLandingComponent,
    PricingComponent,
    ResourcesComponent,
    WelcomePostComponent,
    ScheduleLandingComponent,
    PreviewClassComponent,
    TopBarComponent,
    SettingsComponent,
    OtherProfileComponent,
    AvailabilityComponent,
    PageNotFoundComponent,
    LearnerCompleteSignupComponent,
    ClassSearchComponent,
    FreeClassComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SocialLoginModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    SnotifyModule,
    NgMultiSelectDropDownModule.forRoot(),
    HttpClientModule,
    EditorModule,
    ImageCropperModule,
    NgxFileDropModule,
    Ng5SliderModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
      defaultLanguage: 'en'
    }),
    BrowserAnimationsModule,
    SlickCarouselModule,
    MatCheckboxModule,
    CalanderModule.forRoot(),
    DateTimePickerModule,
    CheckBoxModule,
  ],
  providers: [
    {provide: 'SnotifyToastConfig', useValue: ToastConfig},
    SnotifyService,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          /*{
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '636926595566-lh9dhi3i0borgjobusvmgfoug07agr17.apps.googleusercontent.com'
            ),
          },*/
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('332914457884925'),
          },
        ],
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
