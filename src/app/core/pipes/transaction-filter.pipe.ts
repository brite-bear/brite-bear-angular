import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'transactionFilter'
})
export class TransactionFilterPipe implements PipeTransform {

  transform(ride , transactionSearch: string) {
    if (ride && ride.length) {
      return ride.filter(item => {
        if (transactionSearch && item.source.toLowerCase().indexOf(transactionSearch.toLowerCase()) === -1
          && transactionSearch && item.destination.toLowerCase().indexOf(transactionSearch.toLowerCase()) === -1
          && transactionSearch && item.total_earning.toLowerCase().indexOf(transactionSearch.toLowerCase()) === -1) {
          return false;
        }
        return true;
      });
    } else {
      return ride;
    }
  }

}
