import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LocalStorageService} from '../services/local-storage.service';
import {Observable} from 'rxjs';

@Injectable()
export class ApiService {

  constructor(private httpClient: HttpClient,
              private storageService: LocalStorageService) {
  }

  Login(inputData): Observable<any> { // login for teacher
    return this.httpClient.post('auth/login', inputData);
  }
  signUp(data): Observable<any> { // new registration for teacher
    return this.httpClient.post('auth/register', data);
  }
  completeSignUp(data): Observable<any> { // second step of new registration for teacher
    return this.httpClient.post('auth/complete-profile', data);
  }
  
  learnercompleteSignUp(data):Observable<any> { // second step of new registration for teacher
    return this.httpClient.post('auth/complete-profile-learner', data);
  }
  
  FreeClassdetails(data): Observable<any> { // second step of new registration for teacher
    return this.httpClient.post('auth/freeClassDetails', data);
  }
  
  addIntro(data): Observable<any> { // add/update teacher's introduction / profile
    return this.httpClient.post('teacher/profile', data);
  }
  uploadRecordedVideo(data): Observable<any> { // add/update teacher's introduction / profile
    return this.httpClient.post('teacher/record-video', data);
  }
  addExperience(data): Observable<any> { // add/update teacher's experience
    return this.httpClient.post('teacher/add-work-experience', data);
  }
  addCertifications(data): Observable<any> { // add/update teacher's certifications
    return this.httpClient.post('teacher/add-certificate', data);
  }
  addAcademic(data): Observable<any> { // add/update teacher's academic details
    return this.httpClient.post('teacher/add-academic', data);
  }
  uploadVideo(data): Observable<any> { // upload video while teacher is applying
    const fd = new FormData();
    fd.append('file', data);
    return this.httpClient.post('teacher/video-upload', fd);
  }
  getIntroduction(): Observable<any> { // get logged-in teacher's introduction data which is saved
    return this.httpClient.get('teacher/get-profile');
  }
  getTeacherIntro(id):Observable<any> { // get logged-in teacher's introduction data which is saved
    return this.httpClient.get('teacher/get-teacher-profile/'+id);
  }
  
  getIntroTopics(): Observable<any> { // get topics that logged-in teachers wants to teach while getting teacher's introduction
    return this.httpClient.get('teacher/get-profile-topics');
  }
  getTeacherIntroTopics(id): Observable<any> { // get topics that logged-in teachers wants to teach while getting teacher's introduction
    return this.httpClient.get('teacher/get-teacher-profile-topics/'+id);
  }
  getWorkExperience(): Observable<any> { // get work experience of logged-in teacher which is saved
    return this.httpClient.get('teacher/get-work-experience');
  }
  getTeacherWorkExperience(id):Observable<any> { // get work experience of logged-in teacher which is saved
    return this.httpClient.get('teacher/get-teacher-work-experience/'+id);
  }
  getCertificate(): Observable<any> { // get certificates of the logged-in teacher
    return this.httpClient.get('teacher/get-certificate');
  }
  getTeacherCertificate(id): Observable<any> { // get certificates of the logged-in teacher
    return this.httpClient.get('teacher/get-teacher-certificate/'+id);
  }
  getAcademics(): Observable<any> { // get academics data of the logged-in teacher
    return this.httpClient.get('teacher/get-academic');
  }
  getTeacherAcademics(id): Observable<any> { // get academics data of the logged-in teacher
    return this.httpClient.get('teacher/get-teacher-academic/'+id);
  }
  getTopicsByKeyword(data): Observable<any> { // get topics by keywords
    return this.httpClient.post('common/get-topic-by-keyword', data);
  }
  deleteCertificate(id): Observable<any> { // delete certificate that are added
    return this.httpClient.delete('teacher/delete-certificate/' + id);
  }
  deleteExperience(id): Observable<any> { // delete certificate that are added
    return this.httpClient.delete('teacher/delete-work-experience/' + id);
  }
  deleteAcademics(id): Observable<any> { // delete academics that are added
    return this.httpClient.delete('teacher/delete-academic/' + id);
  }
  deleteAddedTopics(id): Observable<any> { // delete added topics that are added
    return this.httpClient.delete('teacher/delete-topic/' + id);
  }
  getSingleWorkExperience(id): Observable<any> { // get single work experience to edit single exp while updating
    return this.httpClient.get('teacher/get-work-experience/' + id);
  }
  getSingleCertificate(id): Observable<any> { // get single Certificate to edit single certificate while updating
    return this.httpClient.get('teacher/get-certificate/' + id);
  }
  getSingleAcademics(id): Observable<any> { // get single Academic to edit single academic while updating
    return this.httpClient.get('teacher/get-academic/' + id);
  }
  updatePassword(data): Observable<any> { // update password form profile settings
    return this.httpClient.post('auth/update-password', data);
  }
  

  getCountries(): Observable<any> { // get countries
    return this.httpClient.get('common/get-countries');
  }
  getStates(id): Observable<any> { // get states by id
    return this.httpClient.get('common/get-states/' + id);
  }
  getCities(id): Observable<any> { // get cities by id
    return this.httpClient.get('common/get-cities/' + id);
  }
  getrole(): Observable<any> { // get personal details
    return this.httpClient.get('auth/getrole');
  }
  getid(): Observable<any> { // get personal details
    return this.httpClient.get('auth/getid');
  }
  getPersonalDetails(): Observable<any> { // get personal details
    return this.httpClient.get('teacher/personal-info');
  }
  getTeacherPersonalDetails(id): Observable<any> { // get personal details
    return this.httpClient.get('teacher/teacher-personal-info/'+id);
  }

  getClassTeacherDetails(id):Observable<any> { // get teacher details
    return this.httpClient.get('teacher/get-class-teacher/'+id);
  } 

  validateFb(data): Observable<any> { // login / signup with fb
    return this.httpClient.post('auth/validate-fb', data);
  }
  validateGoogle(data): Observable<any> { // login / signup with google
    return this.httpClient.post('auth/validate-google', data);
  }
  updateProfile(data): Observable<any> { // update profile from user profile settings
    return this.httpClient.post('auth/update-profile', data);
  }
  forgetPassword(data): Observable<any> { // forget Password
    return this.httpClient.post('auth/forget-password', data);
  }
  resetPassword(data): Observable<any> { // reset Password
    return this.httpClient.post('auth/reset-password', data);
  }

  ////////////////////////////// CLASS MODULE API"S //////////////////////////////////
  getSubjects(): Observable<any> { // get all subjects for class
    return this.httpClient.get('class/get-subjects');
  }
  getClassTypes(): Observable<any> { // get class types while entering class details
    return this.httpClient.get('class/get-types');
  }
  getClassDuration(id): Observable<any> { // get class duration options according to selected class type id
    return this.httpClient.get('class/get-duration/' + id);
  }
  getClassDurationType(id): Observable<any> { // get class duration type according to selected class duration id
    return this.httpClient.get('class/get-duration-type/' + id);
  }
  getClassDurationTime(id): Observable<any> { // get class duration time according to selected class duration id
    return this.httpClient.get('class/get-duration-time/' + id);
  }
  AddClass(data): Observable<any> { // add class or save class as draft on every step of class
    return this.httpClient.post('class/add', data);
  }
  getAddedClass(id): Observable<any> { // get class / class details by sending id
    return this.httpClient.get('class/get/' + id);
  }
  uploadClassFiles(data): Observable<any> { // upload cover image
    const fd = new FormData();
    fd.append('file', data);
    return this.httpClient.post('class/file-upload', fd);
  }
  
  getteacherstatus():Observable<any> { // get class / class details by sending id
    return this.httpClient.get('teacher/status');
  }

  updateTeacherapp(): Observable<any> { // get class / class details by sending id
    return this.httpClient.get('teacher/appUpdate');
  }
  
  uploadTeacherProfile(data): Observable<any> { // upload profile image
    const fd = new FormData();
    fd.append('file', data);
    return this.httpClient.post('teacher/profile-image', fd);
  }
  uploadCroppedImage(data): Observable<any> { // upload profile image
    const fd = new FormData();
    fd.append('file', data);
    return this.httpClient.post('class/file-upload-crop', fd);
  }
  requestListing(data): Observable<any> { // request Listing and add class
    return this.httpClient.post('class/request-listing', data);
  }
  getClassList(): Observable<any> { // get all list of classes either drafts or saved or published
    return this.httpClient.get('class/listall');
  }

  getClassListByTeacher(): Observable<any> { // get all list of classes either drafts or saved or published
    return this.httpClient.get('class/listbyteacher');
  }

  schedulingclass(id , date): Observable<any> {

    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();

    var start_hour=date.getHours();
    var start_min=date.getMinutes();

    const scheduleDate=yyyy+'-'+mm+'-'+dd

    let data: Object = {"id" : id, "start_min": start_min, "start_hour": start_hour , "date": scheduleDate };
    
    return this.httpClient.post('class/schedule', data);
  }
  ////////////////////////////// AVAILABILITY MODULE API"S //////////////////////////////////

  getAvailability(): Observable<any> { // Get Availability Slots
    return this.httpClient.get('teacher/availability');
  }
  sendAvailability(data): Observable<any> { // teacher send his/her Available Slots
    return this.httpClient.post('teacher/availability', data);
  }
}
