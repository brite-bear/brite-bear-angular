import { TestBed } from '@angular/core/testing';

import { ClassFilterService } from './class-filter.service';

describe('ClassFilterService', () => {
  let service: ClassFilterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClassFilterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
