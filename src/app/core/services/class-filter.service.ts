import { Injectable } from '@angular/core';
import { element } from 'protractor';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClassFilterService {

  data: Object = {};

  constructor(private httpClient: HttpClient) { }

  /*lowtohighprice = [
    {'price': 5 , 'name': 'class1'},
    {'price': 15 , 'name': 'class2'},
    {'price': 25 , 'name': 'class3'},
    {'price': 35 , 'name': 'class4'},
    {'price': 45 , 'name': 'class5'},
  ];

  getpricefilter(price , age, sub){
    const data: Object = {price , age, sub};
    if(sub.used)
      this.lowtohighprice.push({'price': price.highval , 'name':sub.subj});
    return this.lowtohighprice;
  }*/

  getpricefilter(price , age, sub, countries): Observable<any> {
    this.data = {price , age , sub, countries};
    console.log(this.data , "from service");
    return this.httpClient.post('class/filter' , this.data);
  }
}
