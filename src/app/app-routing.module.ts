import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {LoginComponent} from './components/login/login.component';
import {SignupComponent} from './components/signup/signup.component';
import {AuthGuardService} from './core/guards/auth.guard.service';
import {CompleteSignupComponent} from './components/complete-signup/complete-signup.component';
import {TermsConditionsComponent} from './components/terms-conditions/terms-conditions.component';
import {BlogComponent} from './components/blog/blog.component';
import {ContactUsComponent} from './components/contact-us/contact-us.component';
import {AboutComponent} from './components/about/about.component';
import {VisionComponent} from './components/vision/vision.component';
import {FreeClassComponent} from './components/free-class/free-class.component';
import {NewsComponent} from './components/news/news.component';
import {OurTeachersComponent} from './components/our-teachers/our-teachers.component';
import {GalleryComponent} from './components/gallery/gallery.component';
import {FaqComponent} from './components/faq/faq.component';
import {ActivityComponent} from './components/activity/activity.component';
import {ProfileViewComponent} from './components/teaching/teaching-component/profile/profile-view/profile-view.component';
import {SettingsComponent} from './components/teaching/teaching-component/settings/settings.component';
import {OtherProfileComponent} from './components/teaching/teaching-component/profile/other-profile/other-profile.component';
import {PreviewClassComponent} from './components/teaching/teaching-component/preview-class/preview-class.component';
import {TopBarComponent} from './components/teaching/teaching-component/top-bar/top-bar.component';
import {LandingComponent} from './components/teaching/teaching-component/apply/landing/landing.component';
import {IntroductionComponent} from './components/teaching/teaching-component/apply/introduction/introduction.component';
import {ExperienceComponent} from './components/teaching/teaching-component/apply/experience/experience.component';
import {CertificateComponent} from './components/teaching/teaching-component/apply/certificate/certificate.component';
import {AcademicsComponent} from './components/teaching/teaching-component/apply/academics/academics.component';
import {ClassLandingComponent} from './components/teaching/teaching-component/class/class-landing/class-landing.component';
import {ClassDetailsComponent} from './components/teaching/teaching-component/class/class-details/class-details.component';
import {CourseFormatComponent} from './components/teaching/teaching-component/class/course-format/course-format.component';
import {CoverImageComponent} from './components/teaching/teaching-component/class/cover-image/cover-image.component';
import {DescriptionComponent} from './components/teaching/teaching-component/class/description/description.component';
import {AdditionalInfoComponent} from './components/teaching/teaching-component/class/additional-info/additional-info.component';
import {ResourcesComponent} from './components/teaching/teaching-component/class/resources/resources.component';
import {PricingComponent} from './components/teaching/teaching-component/class/pricing/pricing.component';
import {WelcomePostComponent} from './components/teaching/teaching-component/class/welcome-post/welcome-post.component';
import {AvailabilityComponent} from './components/teaching/teaching-component/availability/availability.component';
import {ScheduleLandingComponent} from './components/teaching/teaching-component/schedule/schedule-landing/schedule-landing.component';
import {PaymentLandingComponent} from './components/teaching/teaching-component/payment/payment-landing/payment-landing.component';
import {NoAuthGuardService} from './core/guards/no-auth.guard.service';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { LearnerCompleteSignupComponent } from './components/learner-complete-signup/learner-complete-signup.component';
import {ClassSearchComponent} from './components/class-search/class-search.component'
import {CalanderModule} from "../../projects/calander/src/app/app.module";
import {AppComponent} from "../../projects/calander/src/app/app.component"


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'freeClass',component:FreeClassComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'complete-signUp', component: CompleteSignupComponent, canActivate: [AuthGuardService]},
  {path:'learnerCompleteSignup',component:LearnerCompleteSignupComponent},
  {path: 'blog', component: BlogComponent},
  {path: 'contact', component: ContactUsComponent},
  {path: 'terms', component: TermsConditionsComponent},
  {path: 'about', component: AboutComponent},
  {path: 'vision', component: VisionComponent},
  {path: 'news', component: NewsComponent},
  {path: 'faq', component: FaqComponent},
  {path: 'activity', component: ActivityComponent},
  {path: 'our-teachers', component: OurTeachersComponent},
  {path: 'gallery', component: GalleryComponent},
  {path: 'profile', component: ProfileViewComponent, canActivate: [AuthGuardService]},
  {path: 'settings', component: SettingsComponent, canActivate: [AuthGuardService]},
  {path: 'others-profile/:id', component: OtherProfileComponent},
  {path: 'preview/:id', component: PreviewClassComponent},
  {path: 'teacher', component: TopBarComponent, canActivate: [AuthGuardService],
    children: [
  {path: 'apply', component: LandingComponent, canActivate: [AuthGuardService]},
  {path: 'apply/introduction', component: IntroductionComponent, canActivate: [AuthGuardService]},
  {path: 'apply/experience', component: ExperienceComponent, canActivate: [AuthGuardService]},
  {path: 'apply/certifications', component: CertificateComponent, canActivate: [AuthGuardService]},
  {path: 'apply/academic', component: AcademicsComponent, canActivate: [AuthGuardService]},
  {path: 'class', component: ClassLandingComponent, canActivate: [AuthGuardService]},
  {path: 'class/class-details/:id', component: ClassDetailsComponent, canActivate: [AuthGuardService]},
  {path: 'class/class-details', component: ClassDetailsComponent, canActivate: [AuthGuardService]},
  {path: 'class/course/:id', component: CourseFormatComponent, canActivate: [AuthGuardService]},
  {path: 'class/image/:id', component: CoverImageComponent, canActivate: [AuthGuardService]},
  {path: 'class/description/:id', component: DescriptionComponent, canActivate: [AuthGuardService]},
  {path: 'class/extra/:id', component: AdditionalInfoComponent, canActivate: [AuthGuardService]},
  {path: 'class/resources/:id', component: ResourcesComponent, canActivate: [AuthGuardService]},
  {path: 'class/pricing/:id', component: PricingComponent, canActivate: [AuthGuardService]},
  {path: 'class/welcome/:id', component: WelcomePostComponent, canActivate: [AuthGuardService]},
  {path: 'availability', component: AvailabilityComponent, canActivate: [AuthGuardService]},
  {path: 'schedule', component: ScheduleLandingComponent, canActivate: [AuthGuardService], 
    children: [{path: 'calander', component: AppComponent}]
  },
  {path: 'pay', component: PaymentLandingComponent, canActivate: [AuthGuardService]},
]},
/*{path: 'teacher', loadChildren: () =>
      import('./components/teaching/teaching.module').then(m => m.TeachingModule), canActivate: [AuthGuardService]},*/
      {path: 'class-search', component: ClassSearchComponent},
      {path: '**', component: PageNotFoundComponent}];


@NgModule({
  imports: [
    RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload'}),
    CalanderModule.forRoot()
],
  // imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
