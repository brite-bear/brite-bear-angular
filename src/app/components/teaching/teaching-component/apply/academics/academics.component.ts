import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../../../core/http/api.service';
import {Router} from '@angular/router';
import {SnotifyService} from 'ng-snotify';
declare var $: any;
@Component({
  selector: 'app-academics',
  templateUrl: './academics.component.html',
  styleUrls: ['./academics.component.css']
})
export class AcademicsComponent implements OnInit {
  academicObj = {
    id: '',
    school_name: '',
    area_of_study: '',
    degree: '',
    start_year: '',
    end_year: '',
    terms_accepted: true
  };
  loader = false;
  academicsData;
  academicsAllData = [];
  singleAcademicsData;
  maximumDateStart;
  constructor(private apiServices: ApiService,
              private router: Router,
              private snotifyService: SnotifyService) {
    $('html, body').animate(
      {scrollTop: '350'}, 800);
  }

  ngOnInit(): void {
    $('.selectpicker').selectpicker('refresh');
    this.maximumDateStart = new Date();
    var d = new Date(this.maximumDateStart),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    console.log([year, month, day].join('-'));
    this.maximumDateStart = [year, month, day].join('-');
    this.getAcademics();
  }

  addAcademic() {
    this.loader = true;
    if (this.academicObj.id !== '') {
      this.apiServices.addAcademic(this.academicObj).subscribe(res => {
        this.loader = false;
        // this.router.navigate(['teacher/apply']);
        // this.snotifyService.success('Congratulations you\'ve completed your profile.' +
        // It will take few hours to verify your profile' , 'Success');
        this.academicObj.area_of_study = '';
        this.academicObj.school_name = '';
        this.academicObj.degree = '';
        this.academicObj.end_year = '';
        this.academicObj.start_year = '';
        this.academicObj.id = '';
        $('select[name=start_year]').val(this.academicObj.start_year);
        $('select[name=end_year]').val(this.academicObj.end_year);
        $('.selectpicker').selectpicker('refresh');
        this.getAcademics();
      }, (error) => {
        this.loader = false;
        this.snotifyService.error(error.error.message, 'Error');
      });
    }
    if (this.academicObj.id === '') {
      const data = {
        school_name: this.academicObj.school_name,
        area_of_study: this.academicObj.area_of_study,
        degree: this.academicObj.degree,
        start_year: this.academicObj.start_year,
        end_year: this.academicObj.end_year,
      };
      this.apiServices.addAcademic(data).subscribe(res => {
        this.loader = false;
        this.academicObj.area_of_study = '';
        this.academicObj.school_name = '';
        this.academicObj.degree = '';
        this.academicObj.end_year = '';
        this.academicObj.start_year = '';
        $('select[name=start_year]').val(this.academicObj.start_year);
        $('select[name=end_year]').val(this.academicObj.end_year);
        $('.selectpicker').selectpicker('refresh');
        this.getAcademics();
      }, (error) => {
        this.loader = false;
        this.snotifyService.error(error.error.message, 'Error');
      });
    }
  }
  CheckDetailsAndInitiate() {
    console.log(this.academicObj.terms_accepted);
    if (this.academicObj.terms_accepted === false) {
      this.snotifyService.error('Please Check our Terms And Condition');
    } else  if (!this.academicsAllData[0]) {
      this.snotifyService.error('Please Fill All The Above Details And Save Before Initiating The Process');
    } else if (this.academicsAllData[0] && this.academicObj.terms_accepted === true) {
      
      this.apiServices.updateTeacherapp().subscribe(res=>{
        
      });

      this.router.navigate(['teacher/class']);
      this.snotifyService.success('Congratulations you\'ve completed your profile. ' +
        'It will take few hours to verify your profile' , 'Success');
    }
  }
  
  getAcademics() {
    this.apiServices.getAcademics().subscribe(res => {
      this.academicsAllData = res.response;
      this.academicsData = res.response[0];
 /*     this.academicObj.school_name = this.academicsData.instituteName;
      this.academicObj.area_of_study = this.academicsData.guideName;
      this.academicObj.degree = this.academicsData.certificateTitle;
      this.academicObj.start_year = this.academicsData.startYear;
      this.academicObj.end_year = this.academicsData.endYear;
      $('select[name=start_year]').val(this.academicsData.startYear);
      $('select[name=end_year]').val(this.academicsData.endYear);
      $('.selectpicker').selectpicker('refresh');*/
    });
  }
  deleteAcademics(id) {
    this.snotifyService.confirm('Click Yes if You Want Delete this Academic', 'Are You Sure', {
      timeout: 2000,
      closeOnClick: false,
      buttons: [
        {
          text: 'Yes', action: () =>
            this.apiServices.deleteAcademics(id).subscribe((res) => {
                this.snotifyService.success('Deleted', 'Success', {
                  timeout: 2000,
                  showProgressBar: false,
                  closeOnClick: false,
                  pauseOnHover: true
                });
                this.getAcademics();
              },
              (err) => {
                console.log(err);
              })
          , bold: false
        },
        {
          text: 'No', action: () =>
            this.snotifyService.remove()
        },
      ]
    });
  }
  getSingleAcademics(id) {
    this.apiServices.getSingleAcademics(id).subscribe( res => {
      this.singleAcademicsData = res.response[0];
      console.log(this.singleAcademicsData.id);
      this.academicObj.id = this.singleAcademicsData.id;
      this.academicObj.school_name = this.singleAcademicsData.schoolName;
      this.academicObj.degree = this.singleAcademicsData.degree;
      this.academicObj.area_of_study = this.singleAcademicsData.areaOfStudy;
      this.academicObj.start_year = this.singleAcademicsData.startYear;
      this.academicObj.end_year = this.singleAcademicsData.endYear;
      $('select[name=start_year]').val(this.singleAcademicsData.startYear);
      $('select[name=end_year]').val(this.singleAcademicsData.endYear);
      $('select[name=validity]').val(this.singleAcademicsData.validity);
      $('.selectpicker').selectpicker('refresh');
    });
  }
  guide() {
    window.open('terms');
  }
}
