import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../../../core/http/api.service';
import {LocalStorageService} from '../../../../../core/services/local-storage.service';
import {SnotifyService} from 'ng-snotify';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  
  isapplied;
  
  constructor(private apiServices: ApiService,
    private auth: LocalStorageService,
    private snotifyService: SnotifyService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getstatus();
  }

  getstatus(){
    this.apiServices.getteacherstatus().subscribe( res => {
      this.isapplied=res.response.rows[0].isapplied;
    });
  }

}
