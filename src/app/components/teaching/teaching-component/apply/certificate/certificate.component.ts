import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../../../core/http/api.service';
import {Router} from '@angular/router';
import {SnotifyService} from 'ng-snotify';
declare var $: any;

@Component({
  selector: 'app-certificate',
  templateUrl: './certificate.component.html',
  styleUrls: ['./certificate.component.css']
})
export class CertificateComponent implements OnInit {
certObj = {
  id: '',
  institute_name: '',
  certificate_title: '',
  guide_name: '',
  start_year: '',
  end_year: '',
  valid_year: '',
  valid_month: ''
};
  loader = false;
  certificateData;
  certificateAllData = [];
  singleCertiData;
  maximumDateStart;
  constructor(private apiServices: ApiService,
              private router: Router,
              private snotifyService: SnotifyService) {
    $('html, body').animate(
      { scrollTop: '350' }, 800);
  }

  ngOnInit(): void {
    $('.selectpicker').selectpicker('refresh');
    $('.selectpicker').selectpicker('refresh');
    this.maximumDateStart = new Date();
    var d = new Date(this.maximumDateStart),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    console.log([year, month, day].join('-'));
    this.maximumDateStart = [year, month, day].join('-');
    this.getCertificate();
  }
  addCertifications() {
    this.loader = true;
    if (this.certObj.id !== '') {
      this.apiServices.addCertifications(this.certObj).subscribe(res => {
        this.loader = false;
        this.certObj.certificate_title = '';
        this.certObj.institute_name = '';
        this.certObj.guide_name = '';
        this.certObj.valid_year = '';
        this.certObj.valid_month = '';
        this.certObj.end_year = '';
        this.certObj.start_year = '';
        this.certObj.id = '';
        $('select[name=start_year]').val(this.certObj.start_year);
        $('select[name=end_year]').val(this.certObj.end_year);
        $('.selectpicker').selectpicker('refresh');
        // this.router.navigate(['teacher/apply/academic']);
        this.getCertificate();
      }, (error) => {
        this.loader = false;
        this.snotifyService.error(error.error.message, 'Error');
      });
    }
    if (this.certObj.id === '') {
      const data = {
          institute_name: this.certObj.institute_name,
          certificate_title: this.certObj.certificate_title,
          guide_name: this.certObj.guide_name,
          start_year: this.certObj.start_year,
          end_year: this.certObj.end_year,
          valid_year: this.certObj.valid_year,
          valid_month: this.certObj.valid_month,
        };
      this.apiServices.addCertifications(data).subscribe(res => {
        this.loader = false;
        this.certObj.certificate_title = '';
        this.certObj.institute_name = '';
        this.certObj.guide_name = '';
        this.certObj.valid_year = '';
        this.certObj.valid_month = '';
        this.certObj.end_year = '';
        this.certObj.start_year = '';
        $('select[name=start_year]').val(this.certObj.start_year);
        $('select[name=end_year]').val(this.certObj.end_year);
        $('.selectpicker').selectpicker('refresh');
        this.getCertificate();
      }, (error) => {
        this.loader = false;
        this.snotifyService.error(error.error.message, 'Error');
      });
      }
  }
  getCertificate() {
    this.apiServices.getCertificate().subscribe(res => {
      this.certificateAllData = res.response;
      this.certificateData = res.response[0];
      /*this.certObj.institute_name = this.certificateData.instituteName;
      this.certObj.guide_name = this.certificateData.guideName;
      this.certObj.certificate_title = this.certificateData.certificateTitle;
      this.certObj.start_year = this.certificateData.startYear;
      this.certObj.end_year = this.certificateData.endYear;
      this.certObj.validity = this.certificateData.validity;
      $('select[name=start_year]').val(this.certificateData.startYear);
      $('select[name=end_year]').val(this.certificateData.endYear);
      $('select[name=validity]').val(this.certificateData.validity);
      $('.selectpicker').selectpicker('refresh');*/
    });
  }
  deleteCertificate(id) {
    this.snotifyService.confirm('Click Yes if You Want Delete this Certificate', 'Are You Sure', {
      timeout: 2000,
      closeOnClick: false,
      buttons: [
        {
          text: 'Yes', action: () =>
            this.apiServices.deleteCertificate(id).subscribe((res) => {
                this.snotifyService.success('Deleted', 'Success', {
                  timeout: 2000,
                  showProgressBar: false,
                  closeOnClick: false,
                  pauseOnHover: true
                });
                this.getCertificate();
              },
              (err) => {
                console.log(err);
              })
          , bold: false
        },
        {
          text: 'No', action: () =>
            this.snotifyService.remove()
        },
      ]
    });
  }
  getSingleCertificate(id) {
    this.apiServices.getSingleCertificate(id).subscribe( res => {
      this.singleCertiData = res.response[0];
      console.log(this.singleCertiData.id);
      this.certObj.id = this.singleCertiData.id;
      this.certObj.institute_name = this.singleCertiData.instituteName;
      this.certObj.guide_name = this.singleCertiData.guideName;
      this.certObj.certificate_title = this.singleCertiData.certificateTitle;
      this.certObj.start_year = this.singleCertiData.startYear;
      this.certObj.end_year = this.singleCertiData.endYear;
      this.certObj.valid_month = this.singleCertiData.validMonth;
      this.certObj.valid_year = this.singleCertiData.validYear;
      $('select[name=start_year]').val(this.certificateData.startYear);
      $('select[name=end_year]').val(this.certificateData.endYear);
      $('select[name=validity]').val(this.certificateData.validity);
      $('.selectpicker').selectpicker('refresh');
    });
  }
  guide() {
    window.open('terms');
  }

}
