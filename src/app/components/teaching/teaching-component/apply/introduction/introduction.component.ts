import {Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from '../../../../../core/http/api.service';
import {Router} from '@angular/router';
import {SnotifyService} from 'ng-snotify';
import {LocalStorageService} from '../../../../../core/services/local-storage.service';
import {IDropdownSettings} from 'ng-multiselect-dropdown/multiselect.model';
import 'video.js/dist/video-js.min.css';
import 'webrtc-adapter';

declare var videojs: any;
declare var $: any;

@Component({
  selector: 'app-introduction',
  templateUrl: './introduction.component.html',
  styleUrls: ['./introduction.component.css']
})
export class IntroductionComponent implements OnInit {
  @ViewChild('videoElement') videoElement: any;
  video: any;
  introObj: any = {
    topics: [],
    work_description: '',
    intro_video: '',
    intro_images: ''
  };
  loader = false;
  videoChangedEvent;
  teacherIntroductionData = {
    workDescription: '',
    introVideo: ''
  };
  teacherTopics = [];
  selectedItems = [];
  addedTeacherTopics = [];
  keywords = '';
  dropdownSettings: IDropdownSettings = {};
  placeholderTinyMce = 'Describe';

  videoProgress = false;
  videoSuccess = false;
  videoProgressText = '';

  constructor(private apiServices: ApiService,
              private router: Router,
              private snotifyService: SnotifyService, private auth: LocalStorageService) {
    $('html, body').animate(
      {scrollTop: '350'}, 800);
  }

  ngOnInit(): void {
    const player = videojs('myVideo', {
      controls: true,
      loop: false,
      // dimensions of video.js player
      fluid: false,
      width: 450,
      height: 350,
      plugins: {
        record: {
          maxLength: 90,
          debug: true,
          audio: true,
          video: {
            // video constraints: set resolution of camera
            mandatory: {
              minWidth: 1280,
              minHeight: 720,
            },
          },
          // dimensions of captured video frames
          frameWidth: 1280,
          frameHeight: 720
        }
      }
    });
    $('.selectpicker').selectpicker('refresh');
    this.getIntroduction();
    this.getTopics();
    this.getAddedTopics();
    // this.video = this.videoElement.nativeElement;
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'topic',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true,
      enableCheckAll: false
    };
    const that = this;
    // tslint:disable-next-line:only-arrow-functions
    player.on('startRecord', function() {
      that.videoProgressText = '';
      that.videoProgress = false;
      console.log('started recording!');
    });

    player.on('finishRecord', function() {
      // player.record().saveAs({video: 'my-video-file-name.webm'});
      that.loader = false;
      const data = player.recordedData;
      const formData = new FormData();
      formData.append('file', data, data.name);

      that.videoProgress = true;
      that.videoProgressText = 'Please wait, Video upload is in progress...';

      that.apiServices.uploadRecordedVideo(formData).subscribe(res => {
        that.videoProgressText = 'Video uploaded. You can preview using play button or re-record it';
        that.selectVideo(res.response);
        that.introObj.intro_video = res.response;
        that.snotifyService.success('Video uploaded successfully', 'Success');
        that.loader = false;
        that.videoSuccess = true;
        }, (error) => {
        that.loader = false;
        that.snotifyService.error(error.error.message, 'Error');
      });

    });
  }

  /* start() {
      this.initCamera({ video: true, audio: false });
    }
    sound() {
      this.initCamera({ video: true, audio: true });
    }

    initCamera(config: any) {
      const browser = navigator as any;

      browser.getUserMedia = (browser.getUserMedia ||
        browser.webkitGetUserMedia ||
        browser.mozGetUserMedia ||
        browser.msGetUserMedia);

      browser.mediaDevices.getUserMedia(config).then(stream => {
        this.video.src = window.URL.createObjectURL(stream);
        this.video.play();
      });
    }
    pause() {
      this.video.pause();
    }

    toggleControls() {
      this.video.controls = this.displayControls;
      this.displayControls = !this.displayControls;
    }

    resume() {
      this.video.play();
    }*/
  onItemSelect(item) {
    console.log(item);
    this.introObj.topics.push(item.id);
    console.log(this.introObj.topics);
  }

  onItemDeSelect(item) {
    console.log(item);
    this.introObj.topics.splice(this.introObj.topics.indexOf(item.id, 1));
    console.log(this.introObj.topics);
  }

  AddIntro() {
    this.loader = true;
    this.apiServices.addIntro(this.introObj).subscribe(res => {
      this.loader = false;
      this.router.navigate(['teacher/apply/experience']);
      console.log(res);
    }, (error) => {
      this.loader = false;
      this.snotifyService.error(error.error.message, 'Error');
    });
  }

  /*  uploadImage(file) {
      this.loader = true;
      console.log(file.target.files[0].name);
      console.log(file.target.files[0]);
      // if (file.size <= 2097152) {
      this.imageChangedEvent = file.target.files[0].name;
      /!*  const formData = new FormData();
        formData.append('file', file.target.files[0].name);
        console.log(formData);*!/
      console.log(this.imageChangedEvent);
      this.apiServices.uploadImage(this.imageChangedEvent).subscribe(res => {
        this.selectImage(res.response.path);
        this.snotifyService.success('Image successfully uploaded', 'Success');
        this.loader = false;
      }, (error) => {
        this.snotifyService.error(error.error.message, 'Error');
        this.loader = false;
      });
      /!* } else {
         this.snotifyService.error('Oops!! Please select image less than 5 MB', 'Error');
         this.loader = false;
       }*!/
    }*/

  uploadVideo(file) {
    this.loader = true;
    this.videoChangedEvent = file.target.files[0];
    this.apiServices.uploadVideo(this.videoChangedEvent).subscribe(res => {
      this.selectVideo(res.response);
      this.introObj.intro_video = res.response;
      this.snotifyService.success('Video successfully uploaded', 'Success');
      this.loader = false;
      this.videoSuccess = true;
    }, (error) => {
      this.snotifyService.error(error.error.message, 'Error');
      this.loader = false;
    });
  }

  selectVideo(path) {
    this.auth.setValue('intro_video', [path]);
    this.auth.setValue('intro_video_single', path);
  }

  getIntroduction() {
    this.apiServices.getIntroduction().subscribe(res => {
      if (res.response[0]) {
        this.teacherIntroductionData = res.response[0];
        this.introObj.work_description = this.teacherIntroductionData.workDescription;
        this.introObj.intro_video = this.teacherIntroductionData.introVideo;
      }
    });
  }

  getTopics() {
    this.apiServices.getTopicsByKeyword({keyword: this.keywords}).subscribe(res => {
      this.teacherTopics = res.response;
    });
  }

  /* getTopics() {
     this.apiServices.getIntroTopics().subscribe(res => {
       this.teacherTopics = res.response;
     });
   }*/
  getAddedTopics() {
    this.apiServices.getIntroTopics().subscribe(res => {
      if (res.response) {
        this.addedTeacherTopics = res.response;
      }
    });
  }

  deleteAddedTopic(id) {
    this.apiServices.deleteAddedTopics(id).subscribe(res => {
      this.snotifyService.success('Topic Removed Successfully', 'Success');
      this.getAddedTopics();
    });
  }
  guide() {
    window.open('terms');
  }
}
