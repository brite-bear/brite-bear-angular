import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../../../core/http/api.service';
import {Router} from '@angular/router';
import {SnotifyService} from 'ng-snotify';
declare var $: any;
@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.css']
})
export class ExperienceComponent implements OnInit {
  expObj = {
    id: '',
    organization_name: '',
    role: '',
    location: '',
    country: '',
    state: '',
    start_year: '',
    end_year: ''
  };
  workExperienceData;
  workExperienceAllData = [];
  loader = false;
  singleExpData;
  maximumDateStart;
  allCountries = [];
  allStates = [];
  currentDate = 0;
  constructor(private apiServices: ApiService,
              private router: Router,
              private snotifyService: SnotifyService) {
    $('html, body').animate(
      { scrollTop: '350' }, 800);
  }

  ngOnInit(): void {
    $('.selectpicker').selectpicker('refresh');
    this.maximumDateStart = new Date();
    var d = new Date(this.maximumDateStart),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    console.log([year, month, day].join('-'));
    this.maximumDateStart = [year, month, day].join('-');
    console.log(this.maximumDateStart);
    this.getWorkExperience();
    this.getCountries();
  }
  saveExperience() {
    this.loader = true;
    if (this.expObj.id !== '') {
      this.apiServices.addExperience(this.expObj).subscribe(res => {
        this.loader = false;
        this.expObj.organization_name = '';
        this.expObj.role = '';
        this.expObj.location = '';
        this.expObj.start_year = '';
        this.expObj.end_year = '';
        this.expObj.id = '';
        this.getWorkExperience();
        // this.router.navigate(['teacher/apply/certifications']);
        console.log(res);
      }, (error) => {
        this.loader = false;
        this.snotifyService.error(error.error.message, 'Error');
      });
    } else if (this.expObj.id === '') {
      const data = {
        organization_name: this.expObj.organization_name,
        role: this.expObj.role,
        location: this.expObj.location,
        start_year: this.expObj.start_year,
        end_year: this.expObj.end_year,
      };
      this.apiServices.addExperience(data).subscribe(res => {
        this.loader = false;
        this.expObj.organization_name = '';
        this.expObj.role = '';
        this.expObj.location = '';
        this.expObj.start_year = '';
        this.expObj.end_year = '';
        this.getWorkExperience();
        // this.router.navigate(['teacher/apply/certifications']);
        console.log(res);
      }, (error) => {
        this.loader = false;
        this.snotifyService.error(error.error.message, 'Error');
      });
    }
  }
  hideEndingDate() {
    console.log(this.currentDate);
  }
  checkDateFormat() {
    console.log(this.expObj.start_year);
  }
  getWorkExperience() {
    this.apiServices.getWorkExperience().subscribe(res => {
      this.workExperienceAllData = res.response;
      this.workExperienceData = res.response[0];
     /* this.expObj.organization_name = this.workExperienceData.organizationName;
      this.expObj.location = this.workExperienceData.location;
      this.expObj.role = this.workExperienceData.role;
      this.expObj.start_year = this.workExperienceData.startYear;
      this.expObj.end_year = this.workExperienceData.endYear;
      $('select[name=start_year]').val(this.workExperienceData.startYear);
      $('select[name=end_year]').val(this.workExperienceData.endYear);
      $('.selectpicker').selectpicker('refresh');*/
    });
  }
  deleteExperience(id) {
    this.snotifyService.confirm('Click Yes if You Want Delete this Experience', 'Are You Sure', {
      timeout: 2000,
      closeOnClick: false,
      buttons: [
        {
          text: 'Yes', action: () =>
            this.apiServices.deleteExperience(id).subscribe((res) => {
                this.snotifyService.success('Deleted', 'Success', {
                  timeout: 2000,
                  showProgressBar: false,
                  closeOnClick: false,
                  pauseOnHover: true
                });
                this.getWorkExperience();
              },
              (err) => {
                console.log(err);
              })
          , bold: false
        },
        {
          text: 'No', action: () =>
            this.snotifyService.remove()
        },
      ]
    });
  }
  getSingleExp(id) {
    this.apiServices.getSingleWorkExperience(id).subscribe( res => {
      this.singleExpData = res.response[0];
      this.expObj.id = this.singleExpData.id;
      this.expObj.organization_name = this.singleExpData.organizationName;
      this.expObj.location = this.singleExpData.location;
      this.expObj.role = this.singleExpData.role;
      this.expObj.start_year = this.singleExpData.startYear;
      this.expObj.end_year = this.singleExpData.endYear;
      $('select[name=start_year]').val(this.singleExpData.startYear);
      $('select[name=end_year]').val(this.singleExpData.endYear);
      $('.selectpicker').selectpicker('refresh');
    });
  }
  getCountries() {
    this.apiServices.getCountries().subscribe(res => {
      this.allCountries = res.response;
    });
  }
  getStates() {
    this.apiServices.getStates(this.expObj.country).subscribe(res => {
      this.allStates = res.response;
    });
  }
  guide() {
    window.open('terms');
  }
}
