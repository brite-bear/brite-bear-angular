import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LocalStorageService} from '../../../../core/services/local-storage.service';
import {environment} from '../../../../../environments/environment';
import {ApiService} from '../../../../core/http/api.service';
import {Router,NavigationEnd} from '@angular/router';

declare var $: any;
@Component({
  selector: 'app-preview-class',
  templateUrl: './preview-class.component.html',
  styleUrls: ['./preview-class.component.css']
})
export class PreviewClassComponent implements OnInit {
  id;
  assetsUrl = environment.assetsUrl;
  classObj: any = {
    id: '',
    title: '',
    summary: '',
    subject_id: '',
    class_type_id: '',
    from_age: 0,
    to_age: 0,
    size_min: 0,
    size_max: 0,
    class_duration: '',
    class_duration_type: '',
    class_duration_time: '',
    cover_image: '',
    cover_video: '',
    description: '',
    assignment: '',
    goal: '',
    assessment: '',
    hours_per_week: 0,
    parent_guidance: '',
    sources: '',
    teacher_expertise: '',
    external_resources: [],
    class_material_title: [],
    class_material_attachments: [],
    assignment_title: [],
    assignment_attachments: [],
    price_per_student: 0,
    welcome_note: '',
    welcome_cover_path: '',
    teacher_id:'',
    rating:'',
    review:''
  };
  savedClassData;
  getSavedClass;

  // .....Local Values.....
  localSubject = '';
  localDurationTime = '';
  localClassType = '';
  localClassTypeID = '';
  profSettingsObj = {
    first_name: '',
    last_name: '',
    email: '',
    phone: '',
    country_id: '',
    region_id: '',
    city_id: '',
    username: '',
    profile_image: '',
    country_iso:'',
    country_name:'',
    city:'',
    state:'',
    about:'',
    rating:''
  };
  accountSettingsObj = {
    email: '',
    phone: '',
    username: ''
  };
  userFirstName;
  userLastName;
  allPersonalDetails = {
  user: [],
    locale: [],
    about:[]
};
  constructor(private route: ActivatedRoute, private router: Router,private auth: LocalStorageService,
              private apiServices: ApiService ) {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.localSubject = this.auth.getValue('selectedSubject', false);
      this.localDurationTime = this.auth.getValue('selectedDurationTime', false);
      this.localClassType = this.auth.getValue('classType', false);
      this.localClassTypeID = this.auth.getValue('classTypeID', false);
      this.profSettingsObj.first_name = this.auth.getValue('firstName', false);
      this.userFirstName = this.auth.getValue('firstName', false);
      this.userLastName = this.auth.getValue('lastName', false);
      this.profSettingsObj.last_name = this.auth.getValue('lastName', false);
      this.accountSettingsObj.email = this.auth.getValue('email', false);
      this.accountSettingsObj.phone = this.auth.getValue('phone', false);
      this.accountSettingsObj.username = this.auth.getValue('username', false);
    });
}
  ngOnInit(): void {
    this.getClass();
    this.getPersonalDetails();
    this.router.events.subscribe((evt) => {
      window.scrollTo(0, 0);
    });
  }
  getPersonalDetails() {
    this.apiServices.getClassTeacherDetails(this.id).subscribe( res => {
      this.allPersonalDetails = res.response;
      this.profSettingsObj.first_name  = this.allPersonalDetails.user[0].firstName;
      this.profSettingsObj.last_name  = this.allPersonalDetails.user[0].lastName;
      this.profSettingsObj.email  = this.allPersonalDetails.user[0].email;
      this.profSettingsObj.phone  = this.allPersonalDetails.user[0].phone;
      this.profSettingsObj.username  = this.allPersonalDetails.user[0].username;
      this.profSettingsObj.profile_image  = this.allPersonalDetails.user[0].image;
      this.profSettingsObj.city_id  = this.allPersonalDetails.locale[0].cityId;
      this.profSettingsObj.region_id = this.allPersonalDetails.locale[0].regionId;
      this.profSettingsObj.country_id = this.allPersonalDetails.locale[0].countryId;
      this.profSettingsObj.country_iso = this.allPersonalDetails.locale[0].country.iso2;
      this.profSettingsObj.country_name = this.allPersonalDetails.locale[0].country.name;
      this.profSettingsObj.city = this.allPersonalDetails.locale[0].city.name;
      this.profSettingsObj.state = this.allPersonalDetails.locale[0].state.name;
      this.profSettingsObj.about = this.allPersonalDetails.about[0].workDescription; 
      this.profSettingsObj.rating=this.allPersonalDetails.about[0].rating;
      
    });
  }
  getClass() {
    this.apiServices.getAddedClass(this.id).subscribe(res => {
      this.getSavedClass = res.response;
      this.classObj.id = this.getSavedClass.id;
      this.classObj.teacher_id=this.getSavedClass.teacher_id;
      this.classObj.title = this.getSavedClass.title;
      this.classObj.summary = this.getSavedClass.summary;
      this.classObj.subject_id = this.getSavedClass.subject;
      this.classObj.class_type_id = this.getSavedClass.classType.id;
      this.classObj.from_age = this.getSavedClass.fromAge;
      this.classObj.to_age = this.getSavedClass.toAge;
      this.classObj.size_min = this.getSavedClass.sizeMin;
      this.classObj.size_max = this.getSavedClass.sizeMax;
      this.classObj.class_duration = this.getSavedClass.classDuration;
      this.classObj.class_duration_type = this.getSavedClass.classDurationType;
      this.classObj.class_duration_time = this.getSavedClass.classDurationTime;
      this.classObj.cover_image = this.getSavedClass.coverImage;
      this.classObj.cover_video = this.getSavedClass.coverVideo;
      this.classObj.description = this.getSavedClass.description;
      this.classObj.assignment = this.getSavedClass.assignment;
      this.classObj.goal = this.getSavedClass.goal;
      this.classObj.assessment = this.getSavedClass.assessment;
      this.classObj.hours_per_week = this.getSavedClass.hoursPerWeek;
      this.classObj.parent_guidance = this.getSavedClass.parentGuidance;
      this.classObj.sources = this.getSavedClass.sources;
      this.classObj.teacher_expertise = this.getSavedClass.teacherExpertise;
      this.classObj.external_resources = this.getSavedClass.externalResource;
      this.classObj.class_material_title = this.getSavedClass.classMaterial;
      this.classObj.class_material_attachments = this.getSavedClass.classMaterialAttachment;
      this.classObj.assignment_title = this.getSavedClass.classAssignment;
      this.classObj.assignment_attachments = this.getSavedClass.classAssingmentAttachment;
      this.classObj.price_per_student = Math.floor(Math.floor(this.getSavedClass.pricePerStudent*73.14)/100)*100+99;;
      this.classObj.welcome_note = this.getSavedClass.welcomeNote;
      this.classObj.welcome_cover_path = this.getSavedClass.welcomeCoverImage;
      this.classObj.rating=this.getSavedClass.rating;
      this.classObj.review=this.getSavedClass.review;
    });

    if(this.classObj.class_duration==1)
        this.localDurationTime='30';
      else
        this.localDurationTime='45'; 
  }
  
  playVideo() {
    $('#playVideo').modal('show');
  }
}
