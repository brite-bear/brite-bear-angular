import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewClassComponent } from './preview-class.component';

describe('PreviewClassComponent', () => {
  let component: PreviewClassComponent;
  let fixture: ComponentFixture<PreviewClassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewClassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
