import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../../../core/http/api.service';
import {LocalStorageService} from '../../../../../core/services/local-storage.service';
import {SnotifyService} from 'ng-snotify';
import { Router } from '@angular/router';

@Component({
  selector: 'app-schedule-landing',
  templateUrl: './schedule-landing.component.html',
  styleUrls: ['./schedule-landing.component.css']
})
export class ScheduleLandingComponent implements OnInit {

  availability = [];
  curdate = new Date();
  constructor(private apiServices: ApiService, private router: Router, private snotify: SnotifyService,
    private auth: LocalStorageService) { }

  ngOnInit(): void {
    //this.getAvailability();
    this.openCalander();
  }

  // getAvailability() {
  //   this.apiServices.getAvailability().subscribe(res => {
  //     this.availability = res.response;
  //     console.log(this.availability)
  //   });
  // }

  openCalander() {
    this.router.navigate(['./teacher/schedule/calander']);
  }

}
