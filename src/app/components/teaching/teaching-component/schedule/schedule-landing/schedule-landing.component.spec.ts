import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleLandingComponent } from './schedule-landing.component';

describe('ScheduleLandingComponent', () => {
  let component: ScheduleLandingComponent;
  let fixture: ComponentFixture<ScheduleLandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleLandingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
