import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../../../core/http/api.service';
import {LocalStorageService} from '../../../../core/services/local-storage.service';
import {SnotifyService} from 'ng-snotify';
declare  var $: any;
@Component({
  selector: 'app-availability',
  templateUrl: './availability.component.html',
  styleUrls: ['./availability.component.css']
})
export class AvailabilityComponent implements OnInit {
  availability = [];
  checkAvailability: any = [];

  constructor(private apiServices: ApiService, private snotify: SnotifyService,
              private auth: LocalStorageService) {
  }

  ngOnInit(): void {
    this.getAvailability();
  }

  getAvailability() {
    this.apiServices.getAvailability().subscribe(res => {
      this.availability = res.response;
    });
  }

  sendAvailability() {
    this.apiServices.sendAvailability({slots: this.availability}).subscribe(res => {
      console.log(res);
      $('#staticBackdrop').modal('hide');
      this.snotify.success('Updated Availability Successfully');

    });
  }
  checkMonday(i, value) {
    if (value.checked == true) {
      value = 1;
      this.availability[i].monday = value;
    } else {
      value = 0;
      this.availability[i].monday = value;
    }
  }
  checkTuesday(i, value) {
    if (value.checked == true) {
      value = 1;
      this.availability[i].tuesday = value;
    } else {
      value = 0;
      this.availability[i].tuesday = value;
    }
  }
  checkWednesday(i, value) {
    if (value.checked == true) {
      value = 1;
      this.availability[i].wednesday = value;
    } else {
      value = 0;
      this.availability[i].wednesday = value;
    }
  }
  checkThursday(i, value) {
    if (value.checked == true) {
      value = 1;
      this.availability[i].thursday = value;
    } else {
      value = 0;
      this.availability[i].thursday = value;
    }
  }
  checkFriday(i, value) {
    console.log(value.checked);
    if (value.checked == true) {
      value = 1;
      this.availability[i].friday = value;
    } else {
      value = 0;
      this.availability[i].friday = value;
    }
  }
  checkSaturday(i, value) {
    if (value.checked == true) {
      value = 1;
      this.availability[i].saturday = value;
    } else {
      value = 0;
      this.availability[i].saturday = value;
    }
  }
  checkSunday(i, value) {
    if (value.checked == true) {
      value = 1;
      this.availability[i].sunday = value;
    } else {
      value = 0;
      this.availability[i].sunday = value;
    }
  }
  SelectAll() {
    for (let i = 0; i < this.availability.length; i++) {
      this.availability[i].monday = 1;
      this.availability[i].tuesday = 1;
      this.availability[i].wednesday = 1;
      this.availability[i].thursday = 1;
      this.availability[i].friday = 1;
      this.availability[i].saturday = 1;
      this.availability[i].sunday = 1;
    }
  }
  DeselectAll() {
    for (let i = 0; i < this.availability.length; i++) {
      this.availability[i].monday = 0;
      this.availability[i].tuesday = 0;
      this.availability[i].wednesday = 0;
      this.availability[i].thursday = 0;
      this.availability[i].friday = 0;
      this.availability[i].saturday = 0;
      this.availability[i].sunday = 0;
    }
  }
}
