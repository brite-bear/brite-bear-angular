import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../../../environments/environment';
import {ActivatedRoute} from '@angular/router';
import {IDropdownSettings} from 'ng-multiselect-dropdown';
import {LocalStorageService} from '../../../../../core/services/local-storage.service';
import {ApiService} from '../../../../../core/http/api.service';
import {SnotifyService} from 'ng-snotify';
import {ImageCroppedEvent} from 'ngx-image-cropper';
declare var $: any;
@Component({
  selector: 'app-other-profile',
  templateUrl: './other-profile.component.html',
  styleUrls: ['./other-profile.component.css']
})
export class OtherProfileComponent implements OnInit {
  id;
  assetsUrl =  environment.assetsUrl;
  userFirstName = '';
  userLastName = '';
  dropdownSettings: IDropdownSettings = {};
  introObj: any = {
    id: '',
    topics: [],
    work_description: '',
    intro_video: '',
    intro_images: ''
  };
  loader = false;
  teacherIntroductionData = {
    id: '',
    workDescription: '',
    introVideo: ''
  };
  addedTeacherTopics = [];
  teacherTopics = [];
  keywords = '';
  workExperienceAllData = [];



  academicsAllData = [];

  certificateAllData = [];
  profSettingsObj = {
    first_name: '',
    last_name: '',
    email: '',
    phone: '',
    country_id: '',
    country_iso: '',
    country_name: '',
    city: '',
    state: '',
    region_id: '',
    city_id: '',
    username: '',
    profile_image: '',
    review:'',
    rating:''
  };
  allCountries = [];
  allStates = [];
  allCities = [];
  croppedImage: any = '';
  image;
  accountSettingsObj = {
    email: '',
    phone: '',
    username: ''
  };
  allPersonalDetails;
  imageChangedEvent;

  constructor(private route: ActivatedRoute,private auth: LocalStorageService, private apiServices: ApiService,
              private snotifyService: SnotifyService) {
    
    this.route.paramMap.subscribe(params => {
          this.id = params.get('id');
                       
        this.profSettingsObj.first_name = this.auth.getValue('firstName', false);
        this.userFirstName = this.auth.getValue('firstName', false);
        this.userLastName = this.auth.getValue('lastName', false);
        this.profSettingsObj.last_name = this.auth.getValue('lastName', false);
        this.accountSettingsObj.email = this.auth.getValue('email', false);
        this.accountSettingsObj.phone = this.auth.getValue('phone', false);
        this.accountSettingsObj.username = this.auth.getValue('username', false);
    })  
  }

  ngOnInit(): void {
    this.getProfile();
    this.getTopicsByKeywords();
    this.getAddedTopics();
    this.getWorkExperience();
    this.getAcademics();
    this.getCertificate();
    this.getPersonalDetails();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'topic',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true,
      enableCheckAll: false
    };
  }
  logout() {
    this.auth.remove(environment.authKey);
    this.auth.remove('firstName');
    this.auth.remove('lastName');
    this.auth.remove('email');
    this.auth.remove('phone');
    this.auth.remove('username');
    window.location.reload();
  }
  getProfile() {
    this.apiServices.getTeacherIntro(this.id).subscribe(res => {
      if (res.response[0]) {
        this.teacherIntroductionData = res.response[0];
        this.introObj.work_description = this.teacherIntroductionData.workDescription;
        this.introObj.intro_video = this.teacherIntroductionData.introVideo;
      }
    });
  }
  getAddedTopics() {
    this.apiServices.getTeacherIntroTopics(this.id).subscribe(res => {
      if (res.response) {
        this.addedTeacherTopics = res.response;
      }
    });
  }

  getTopicsByKeywords() {
    this.apiServices.getTopicsByKeyword({keyword: this.keywords}).subscribe(res => {
      this.teacherTopics = res.response;
    });
  }

  getWorkExperience() {
    this.apiServices.getTeacherWorkExperience(this.id).subscribe(res => {
      this.workExperienceAllData = res.response;
    });
  }


  getAcademics() {
    this.apiServices.getTeacherAcademics(this.id).subscribe(res => {
      this.academicsAllData = res.response;
    });
  }

  getCertificate() {
    this.apiServices.getTeacherCertificate(this.id).subscribe(res => {
      this.certificateAllData = res.response;
    });
  }

  getCountries() {
    this.apiServices.getCountries().subscribe(res => {
      this.allCountries = res.response;
      this.getStates();
    });
  }
  getStates() {
    this.apiServices.getStates(this.profSettingsObj.country_id).subscribe(res => {
      this.allStates = res.response;
      if (this.profSettingsObj.country_id) {
        this.getCities();
      }
    });
  }
  getCities() {
    this.apiServices.getCities(this.profSettingsObj.region_id).subscribe(res => {
      this.allCities = res.response;
    });
  }
  getPersonalDetails() {
    this.apiServices.getTeacherPersonalDetails(this.id).subscribe( res => {
      this.allPersonalDetails = res.response;
      this.profSettingsObj.first_name  = this.allPersonalDetails.user[0].firstName;
      this.profSettingsObj.last_name  = this.allPersonalDetails.user[0].lastName;
      this.profSettingsObj.email  = this.allPersonalDetails.user[0].email;
      this.profSettingsObj.phone  = this.allPersonalDetails.user[0].phone;
      this.profSettingsObj.username  = this.allPersonalDetails.user[0].username;
      this.profSettingsObj.profile_image  = this.allPersonalDetails.user[0].image;
      this.profSettingsObj.city_id  = this.allPersonalDetails.locale[0].cityId;
      this.profSettingsObj.region_id = this.allPersonalDetails.locale[0].regionId;
      this.profSettingsObj.country_id = this.allPersonalDetails.locale[0].countryId;
      this.profSettingsObj.country_iso = this.allPersonalDetails.locale[0].country.iso2;
      this.profSettingsObj.country_name = this.allPersonalDetails.locale[0].country.name;
      this.profSettingsObj.city = this.allPersonalDetails.locale[0].city.name;
      this.profSettingsObj.state = this.allPersonalDetails.locale[0].state.name;
      this.profSettingsObj.review=this.allPersonalDetails.about[0].review;
      this.profSettingsObj.rating=this.allPersonalDetails.about[0].rating;
      this.getCountries();
    });
  }
}
