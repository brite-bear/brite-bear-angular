import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../../../environments/environment';
import {LocalStorageService} from '../../../../../core/services/local-storage.service';
import {IDropdownSettings} from 'ng-multiselect-dropdown';
import {ApiService} from '../../../../../core/http/api.service';
import {SnotifyService} from 'ng-snotify';
import {ImageCroppedEvent} from 'ngx-image-cropper';
declare var $: any;
@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.css']
})
export class ProfileViewComponent implements OnInit {
  assetsUrl =  environment.assetsUrl;
  userFirstName = '';
  userLastName = '';
  dropdownSettings: IDropdownSettings = {};
  introObj: any = {
    id: '',
    topics: [],
    work_description: '',
    intro_video: '',
    intro_images: ''
  };
  loader = false;
  teacherIntroductionData = {
    id: '',
    workDescription: '',
    introVideo: ''
  };
  addedTeacherTopics = [];
  editView = 0;
  teacherTopics = [];
  selectedItems = [];
  keywords = '';

  // exp data
  expView = 0;
  expObj = {
    id: '',
    organization_name: '',
    role: '',
    location: '',
    start_year: '',
    end_year: ''
  };
  workExperienceData;
  workExperienceAllData = [];
  loaderExp = false;
  singleExpData;


// education / academics data
  educationView = 0;
  academicObj = {
    id: '',
    school_name: '',
    area_of_study: '',
    degree: '',
    start_year: '',
    end_year: '',
    terms_accepted: true
  };
  loaderEducation = false;
  academicsData;
  academicsAllData = [];
  singleAcademicsData;

  // certificate data
  certObj = {
    id: '',
    institute_name: '',
    certificate_title: '',
    guide_name: '',
    start_year: '',
    end_year: '',
    valid_year: '',
    valid_month: ''
  };
  loaderCertificate = false;
  certificateData;
  certificateAllData = [];
  singleCertiData;
  certView = 0;
  // profile settings data
  profView = 0;
  profSettingsObj = {
    first_name: '',
    last_name: '',
    email: '',
    phone: '',
    country_id: '',
    region_id: '',
    city_id: '',
    username: '',
    profile_image: ''
  };
  allCountries = [];
  allStates = [];
  allCities = [];
  croppedImage: any = '';
  showCropper = false;
  image;
  FileType;
  // Account settings data
  accountSettingsObj = {
    email: '',
    phone: '',
    username: ''
  };
  allPersonalDetails;
  imageChangedEvent;
  profileImage;
  // Password Update
  passwordObj = {
    old_password: '',
    new_password: '',
    confirm_password: ''
  };
  constructor(private auth: LocalStorageService, private apiServices: ApiService,
              private snotifyService: SnotifyService) {
    this.profSettingsObj.first_name = this.auth.getValue('firstName', false);
    this.userFirstName = this.auth.getValue('firstName', false);
    this.userLastName = this.auth.getValue('lastName', false);
    this.profSettingsObj.last_name = this.auth.getValue('lastName', false);
    this.accountSettingsObj.email = this.auth.getValue('email', false);
    this.accountSettingsObj.phone = this.auth.getValue('phone', false);
    this.accountSettingsObj.username = this.auth.getValue('username', false);
  }

  ngOnInit(): void {
    this.getProfile();
    this.getTopicsByKeywords();
    this.getAddedTopics();
    this.getWorkExperience();
    this.getAcademics();
    this.getCertificate();
    this.getPersonalDetails();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'topic',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true,
      enableCheckAll: false
    };
  }
  logout() {
    this.auth.remove(environment.authKey);
    this.auth.remove('firstName');
    this.auth.remove('lastName');
    this.auth.remove('email');
    this.auth.remove('phone');
    this.auth.remove('username');
    window.location.reload();
  }
  getProfile() {
    this.apiServices.getIntroduction().subscribe(res => {
      if (res.response[0]) {
        this.teacherIntroductionData = res.response[0];
        this.introObj.work_description = this.teacherIntroductionData.workDescription;
        this.introObj.intro_video = this.teacherIntroductionData.introVideo;
      }
    });
  }
  getAddedTopics() {
    this.apiServices.getIntroTopics().subscribe(res => {
      if (res.response) {
        this.addedTeacherTopics = res.response;
      }
    });
  }
  getTopicsByKeywords() {
    this.apiServices.getTopicsByKeyword({keyword: this.keywords}).subscribe(res => {
      this.teacherTopics = res.response;
    });
  }
  onItemSelect(item) {
    console.log(item);
    this.introObj.topics.push(item.id);
    console.log(this.introObj.topics);
  }
  onItemDeSelect(item) {
    console.log(item);
    this.introObj.topics.splice(this.introObj.topics.indexOf(item.id, 1));
    console.log(this.introObj.topics);
  }
  EditIntro() {
    this.editView = 1;
    this.apiServices.getIntroduction().subscribe(res => {
      this.teacherIntroductionData = res.response[0];
      this.introObj.id = this.teacherIntroductionData.id;
      this.introObj.work_description = this.teacherIntroductionData.workDescription;
      this.introObj.intro_video = this.teacherIntroductionData.introVideo;
    });
  }
  SaveIntro() {
    this.editView = 0;
    this.loader = true;
    this.apiServices.addIntro(this.introObj).subscribe(res => {
      this.snotifyService.success('Updated Successfully', 'Success');
      this.loader = false;
      this.getAddedTopics();
    }, (error) => {
      this.loader = false;
      this.snotifyService.error(error.error.message, 'Error');
    });
  }
 /* AddIntro() {
    this.loader = true;
    this.apiServices.addIntro(this.introObj).subscribe(res => {
      this.loader = false;
      this.router.navigate(['teacher/apply/experience']);
      console.log(res);
    }, (error) => {
      this.loader = false;
      this.snotifyService.error(error.error.message, 'Error');
    });
  }*/



 // EXPERIENCE FUNCTIONS BELOW

  saveExperience() {
    this.loaderExp = true;
    this.expView = 0;
    if (this.expObj.id !== '') {
      this.apiServices.addExperience(this.expObj).subscribe(res => {
        this.loaderExp = false;
        this.expObj.organization_name = '';
        this.expObj.role = '';
        this.expObj.location = '';
        this.expObj.start_year = '';
        this.expObj.end_year = '';
        this.expObj.id = '';
        this.snotifyService.success('Updated Successfully', 'Success');
        this.getWorkExperience();
        // this.router.navigate(['teacher/apply/certifications']);
        console.log(res);
      }, (error) => {
        this.loaderExp = false;
        this.snotifyService.error(error.error.message, 'Error');
      });
    } else if (this.expObj.id === '') {
      const data = {
        organization_name: this.expObj.organization_name,
        role: this.expObj.role,
        location: this.expObj.location,
        start_year: this.expObj.start_year,
        end_year: this.expObj.end_year,
      };
      this.apiServices.addExperience(data).subscribe(res => {
        this.loaderExp = false;
        this.expObj.organization_name = '';
        this.expObj.role = '';
        this.expObj.location = '';
        this.expObj.start_year = '';
        this.expObj.end_year = '';
        this.getWorkExperience();
        // this.router.navigate(['teacher/apply/certifications']);
        console.log(res);
      }, (error) => {
        this.loaderExp = false;
        this.snotifyService.error(error.error.message, 'Error');
      });
    }
  }
  getWorkExperience() {
    this.apiServices.getWorkExperience().subscribe(res => {
      this.workExperienceAllData = res.response;
      /* this.expObj.organization_name = this.workExperienceData.organizationName;
       this.expObj.location = this.workExperienceData.location;
       this.expObj.role = this.workExperienceData.role;
       this.expObj.start_year = this.workExperienceData.startYear;
       this.expObj.end_year = this.workExperienceData.endYear;
       $('select[name=start_year]').val(this.workExperienceData.startYear);
       $('select[name=end_year]').val(this.workExperienceData.endYear);
       $('.selectpicker').selectpicker('refresh');*/
    });
  }
  deleteExperience(id) {
    this.snotifyService.confirm('Click Yes if You Want Delete this Experience', 'Are You Sure', {
      timeout: 2000,
      closeOnClick: false,
      buttons: [
        {
          text: 'Yes', action: () =>
            this.apiServices.deleteExperience(id).subscribe((res) => {
                this.snotifyService.success('Deleted', 'Success', {
                  timeout: 2000,
                  showProgressBar: false,
                  closeOnClick: false,
                  pauseOnHover: true
                });
                this.getWorkExperience();
              },
              (err) => {
                console.log(err);
              })
          , bold: false
        },
        {
          text: 'No', action: () =>
            this.snotifyService.remove()
        },
      ]
    });
  }
  getSingleExp(id) {
    this.expView = 1;
    this.apiServices.getSingleWorkExperience(id).subscribe( res => {
      this.singleExpData = res.response[0];
      this.expObj.id = this.singleExpData.id;
      this.expObj.organization_name = this.singleExpData.organizationName;
      this.expObj.location = this.singleExpData.location;
      this.expObj.role = this.singleExpData.role;
      this.expObj.start_year = this.singleExpData.startYear;
      this.expObj.end_year = this.singleExpData.endYear;
      $('select[name=start_year]').val(this.singleExpData.startYear);
      $('select[name=end_year]').val(this.singleExpData.endYear);
      $('.selectpicker').selectpicker('refresh');
    });
  }
  getNewExpView() {
    this.expView = 1;
    this.getWorkExperience();
  }
  experienceEditViewClose() {
    this.expView = 0;
  }

  // Education Academics Functions Below

  addAcademic() {
    this.loaderEducation = true;
    this.educationView = 0;
    if (this.academicObj.id !== '') {
      this.apiServices.addAcademic(this.academicObj).subscribe(res => {
        this.loaderEducation = false;
        // this.router.navigate(['teacher/apply']);
        // this.snotifyService.success('Congratulations you\'ve completed your profile.' +
        // It will take few hours to verify your profile' , 'Success');
        this.academicObj.area_of_study = '';
        this.academicObj.school_name = '';
        this.academicObj.degree = '';
        this.academicObj.end_year = '';
        this.academicObj.start_year = '';
        this.academicObj.id = '';
        $('select[name=start_year]').val(this.academicObj.start_year);
        $('select[name=end_year]').val(this.academicObj.end_year);
        $('.selectpicker').selectpicker('refresh');
        this.snotifyService.success('Updated Successfully', 'Success');
        this.getAcademics();
      }, (error) => {
        this.loaderEducation = false;
        this.snotifyService.error(error.error.message, 'Error');
      });
    }
    if (this.academicObj.id === '') {
      const data = {
        school_name: this.academicObj.school_name,
        area_of_study: this.academicObj.area_of_study,
        degree: this.academicObj.degree,
        start_year: this.academicObj.start_year,
        end_year: this.academicObj.end_year,
      };
      this.apiServices.addAcademic(data).subscribe(res => {
        this.loaderEducation = false;
        this.academicObj.area_of_study = '';
        this.academicObj.school_name = '';
        this.academicObj.degree = '';
        this.academicObj.end_year = '';
        this.academicObj.start_year = '';
        $('select[name=start_year]').val(this.academicObj.start_year);
        $('select[name=end_year]').val(this.academicObj.end_year);
        $('.selectpicker').selectpicker('refresh');
        this.snotifyService.success('Added Successfully', 'Success');
        this.getAcademics();
      }, (error) => {
        this.loaderEducation = false;
        this.snotifyService.error(error.error.message, 'Error');
      });
    }
  }
/*  CheckDetailsAndInitiate() {
    console.log(this.academicObj.terms_accepted);
    if (this.academicObj.terms_accepted === false) {
      this.snotifyService.error('Please Check our Terms And Condition');
    } else  if (!this.academicsAllData[0]) {
      this.snotifyService.error('Please Fill All The Above Details First');
    } else if (this.academicsAllData[0] && this.academicObj.terms_accepted === true) {
      this.router.navigate(['teacher/apply']);
      this.snotifyService.success('Congratulations you\'ve completed your profile. ' +
        'It will take few hours to verify your profile' , 'Success');
    }
  }*/
  getAcademics() {
    this.apiServices.getAcademics().subscribe(res => {
      this.academicsAllData = res.response;
      /*     this.academicObj.school_name = this.academicsData.instituteName;
           this.academicObj.area_of_study = this.academicsData.guideName;
           this.academicObj.degree = this.academicsData.certificateTitle;
           this.academicObj.start_year = this.academicsData.startYear;
           this.academicObj.end_year = this.academicsData.endYear;
           $('select[name=start_year]').val(this.academicsData.startYear);
           $('select[name=end_year]').val(this.academicsData.endYear);
           $('.selectpicker').selectpicker('refresh');*/
    });
  }
  deleteAcademics(id) {
    this.snotifyService.confirm('Click Yes if You Want Delete this Academic', 'Are You Sure', {
      timeout: 2000,
      closeOnClick: false,
      buttons: [
        {
          text: 'Yes', action: () =>
            this.apiServices.deleteAcademics(id).subscribe((res) => {
                this.snotifyService.success('Deleted', 'Success', {
                  timeout: 2000,
                  showProgressBar: false,
                  closeOnClick: false,
                  pauseOnHover: true
                });
                this.getAcademics();
              },
              (err) => {
                console.log(err);
              })
          , bold: false
        },
        {
          text: 'No', action: () =>
            this.snotifyService.remove()
        },
      ]
    });
  }
  getSingleAcademics(id) {
    this.educationView = 1;
    this.apiServices.getSingleAcademics(id).subscribe( res => {
      this.singleAcademicsData = res.response[0];
      console.log(this.singleAcademicsData.id);
      this.academicObj.id = this.singleAcademicsData.id;
      this.academicObj.school_name = this.singleAcademicsData.schoolName;
      this.academicObj.degree = this.singleAcademicsData.degree;
      this.academicObj.area_of_study = this.singleAcademicsData.areaOfStudy;
      this.academicObj.start_year = this.singleAcademicsData.startYear;
      this.academicObj.end_year = this.singleAcademicsData.endYear;
      $('select[name=start_year_aca]').val(this.singleAcademicsData.startYear);
      $('select[name=end_year_aca]').val(this.singleAcademicsData.endYear);
      $('.selectpicker').selectpicker('refresh');
    });
  }
  getNewEducationView() {
    this.educationView = 1;
    this.expObj.id = '';
    $('select[name=start_year_aca]').val(this.expObj.id);
    $('select[name=end_year_aca]').val('');
    $('.selectpicker').selectpicker('refresh');
  }
  educationEditViewClose() {
    this.educationView = 0;
  }
  // Certificate Functions Below

  addCertifications() {
    this.loaderCertificate = true;
    this.certView = 0;
    if (this.certObj.id !== '') {
      this.apiServices.addCertifications(this.certObj).subscribe(res => {
        this.loaderCertificate = false;
        this.certObj.certificate_title = '';
        this.certObj.institute_name = '';
        this.certObj.guide_name = '';
        this.certObj.valid_year = '';
        this.certObj.valid_month = '';
        this.certObj.end_year = '';
        this.certObj.start_year = '';
        this.certObj.id = '';
        $('select[name=start_year]').val(this.certObj.start_year);
        $('select[name=end_year]').val(this.certObj.end_year);
        $('.selectpicker').selectpicker('refresh');
        // this.router.navigate(['teacher/apply/academic']);
        this.getCertificate();
        this.snotifyService.success('Updated Successfully', 'Success');
      }, (error) => {
        this.loaderCertificate = false;
        this.snotifyService.error(error.error.message, 'Error');
      });
    }
    if (this.certObj.id === '') {
      const data = {
        institute_name: this.certObj.institute_name,
        certificate_title: this.certObj.certificate_title,
        guide_name: this.certObj.guide_name,
        start_year: this.certObj.start_year,
        end_year: this.certObj.end_year,
        valid_year: this.certObj.valid_year,
        valid_month: this.certObj.valid_month,
      };
      this.apiServices.addCertifications(data).subscribe(res => {
        this.loaderCertificate = false;
        this.certObj.certificate_title = '';
        this.certObj.institute_name = '';
        this.certObj.guide_name = '';
        this.certObj.valid_year = '';
        this.certObj.valid_month = '';
        this.certObj.end_year = '';
        this.certObj.start_year = '';
        $('select[name=start_year]').val(this.certObj.start_year);
        $('select[name=end_year]').val(this.certObj.end_year);
        $('.selectpicker').selectpicker('refresh');
        this.getCertificate();
        this.snotifyService.success('Added Successfully', 'Success');
      }, (error) => {
        this.loader = false;
        this.snotifyService.error(error.error.message, 'Error');
      });
    }
  }
  getCertificate() {
    this.apiServices.getCertificate().subscribe(res => {
      this.certificateAllData = res.response;
      // this.certificateData = res.response[0];
      /*this.certObj.institute_name = this.certificateData.instituteName;
      this.certObj.guide_name = this.certificateData.guideName;
      this.certObj.certificate_title = this.certificateData.certificateTitle;
      this.certObj.start_year = this.certificateData.startYear;
      this.certObj.end_year = this.certificateData.endYear;
      this.certObj.validity = this.certificateData.validity;
      $('select[name=start_year]').val(this.certificateData.startYear);
      $('select[name=end_year]').val(this.certificateData.endYear);
      $('select[name=validity]').val(this.certificateData.validity);
      $('.selectpicker').selectpicker('refresh');*/
    });
  }
  deleteCertificate(id) {
    this.snotifyService.confirm('Click Yes if You Want Delete this Certificate', 'Are You Sure', {
      timeout: 2000,
      closeOnClick: false,
      buttons: [
        {
          text: 'Yes', action: () =>
            this.apiServices.deleteCertificate(id).subscribe((res) => {
                this.snotifyService.success('Deleted', 'Success', {
                  timeout: 2000,
                  showProgressBar: false,
                  closeOnClick: false,
                  pauseOnHover: true
                });
                this.getCertificate();
              },
              (err) => {
                console.log(err);
              })
          , bold: false
        },
        {
          text: 'No', action: () =>
            this.snotifyService.remove()
        },
      ]
    });
  }
  getSingleCertificate(id) {
    this.certView = 1;
    this.apiServices.getSingleCertificate(id).subscribe( res => {
      this.singleCertiData = res.response[0];
      console.log(this.singleCertiData.id);
      this.certObj.id = this.singleCertiData.id;
      this.certObj.institute_name = this.singleCertiData.instituteName;
      this.certObj.guide_name = this.singleCertiData.guideName;
      this.certObj.certificate_title = this.singleCertiData.certificateTitle;
      this.certObj.start_year = this.singleCertiData.startYear;
      this.certObj.end_year = this.singleCertiData.endYear;
      this.certObj.valid_month = this.singleCertiData.validMonth;
      this.certObj.valid_year = this.singleCertiData.validYear;
      $('select[name=start_year]').val(this.certificateData.startYear);
      $('select[name=end_year]').val(this.certificateData.endYear);
      $('select[name=validity]').val(this.certificateData.validity);
      $('.selectpicker').selectpicker('refresh');
    });
  }
  getNewCertificateView() {
    this.certView = 1;
  }
  certificateEditViewClose() {
    this.certView = 0;
  }
// edit Profile Settings Functions below
  editProfileSettings() {
    this.profView  = 1;
  }
  updateProfileSettings() {
    this.profView = 0;
    this.profSettingsObj.profile_image = this.profileImage;
    this.apiServices.updateProfile(this.profSettingsObj).subscribe( res => {
      console.log(res.response);
      this.getProfile();
      this.getPersonalDetails();
      this.snotifyService.success('Profile Updated Successfully', 'Success');
    });
  }

  uploadProfileImage(file) {
    this.loader = true;
    this.croppedImage=file.target.files[0];
    this.apiServices.uploadTeacherProfile(this.croppedImage).subscribe(res => {
      console.log("Hello",res.response);
      this.selectImage(res.response);
      this.profileImage = res.response;
      //console.log(this.assetsUrl + this.profileImage);
      $('#cropImageModal').modal('hide');
      this.snotifyService.success('Image successfully uploaded', 'Success');
      this.loader = false;
      this.updateProfileSettings();
    }, (error) => {
      this.snotifyService.error(error.error.message, 'Error');
      $('#cropImageModal').modal('hide');
      this.loader = false;
    });
    /*
    this.loader = true;
    // this.imageChangedEvent = this.croppedImage;
    this.apiServices.uploadTeacherProfile(this.croppedImage).subscribe(res => {
      this.selectImage(res.response);
      $('#cropImageModal').modal('hide');
      this.profileImage = res.response;
      this.updateProfileSettings();
      this.loader = false;
    }, (error) => {
      this.snotifyService.error(error.error.message, 'Error');
      $('#cropImageModal').modal('hide');
      this.loader = false;
    });
    */
  }
  selectImage(path) {
    this.auth.setValue('profile_image', [path]);
    this.auth.setValue('profile_image_single', path);
  }

  updatePassword() {
    this.apiServices.updatePassword(this.passwordObj).subscribe( res => {
      this.snotifyService.success('Password Updated Successfully', 'Success');
      this.passwordObj.old_password = '';
      this.passwordObj.new_password = '';
      this.passwordObj.confirm_password = '';
    } , (error) => {
      console.log(error);
      this.snotifyService.error(error.error.err, 'Error');
    });
  }
  deleteAddedTopic(id) {
    this.apiServices.deleteAddedTopics(id).subscribe( res => {
      this.snotifyService.success('Topic Removed Successfully', 'Success');
      this.getAddedTopics();
    });
  }
  getCountries() {
    this.apiServices.getCountries().subscribe(res => {
      this.allCountries = res.response;
      this.getStates();
    });
  }
  getStates() {
    this.apiServices.getStates(this.profSettingsObj.country_id).subscribe(res => {
      this.allStates = res.response;
      if (this.profSettingsObj.country_id) {
        this.getCities();
      }
    });
  }
  getCities() {
    this.apiServices.getCities(this.profSettingsObj.region_id).subscribe(res => {
      this.allCities = res.response;
    });
  }
  getPersonalDetails() {
    this.apiServices.getPersonalDetails().subscribe( res => {
      this.allPersonalDetails = res.response;
      this.profSettingsObj.first_name  = this.allPersonalDetails.user[0].firstName;
      this.profSettingsObj.last_name  = this.allPersonalDetails.user[0].lastName;
      this.profSettingsObj.email  = this.allPersonalDetails.user[0].email;
      this.profSettingsObj.phone  = this.allPersonalDetails.user[0].phone;
      this.profSettingsObj.username  = this.allPersonalDetails.user[0].username;
      this.profSettingsObj.profile_image  = this.allPersonalDetails.user[0].image;
      this.profSettingsObj.city_id  = this.allPersonalDetails.locale[0].cityId;
      this.profSettingsObj.region_id = this.allPersonalDetails.locale[0].regionId;
      this.profSettingsObj.country_id = this.allPersonalDetails.locale[0].countryId;
      this.getCountries();
    });
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    console.log(this.croppedImage);
  }
  /* fileChangeEvent(event: any): void {
     $('#cropImageModal').modal('show');
     this.imageChangedEvent = event;
   }*/
  fileChangeEvent(event: any) {
    this.FileType = event.target.files[0].type;
    console.log(this.FileType);
    if (this.FileType !== 'image/png' && this.FileType !== 'image/jpeg') {
      this.snotifyService.error('Please select .png or jpg or jpeg');
      $('#cropImageModal').modal('hide');
    } else {
      $('#cropImageModal').modal('show');
      this.imageChangedEvent = event;
    }
  }
  imageLoaded() {
    this.showCropper = true;
    console.log('Image loaded');
  }
  cropperReady() {
  }
  loadImageFailed() {
    console.log('Load failed');
  }
}
