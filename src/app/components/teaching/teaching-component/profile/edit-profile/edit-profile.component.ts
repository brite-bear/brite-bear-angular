import { Component, OnInit } from '@angular/core';
import {RouterChangeService} from '../../../../../core/services/router-change.service';
import {environment} from '../../../../../../environments/environment';
import {LocalStorageService} from '../../../../../core/services/local-storage.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  activeUrl;
  userFirstName = '';
  userLastName = '';

  constructor(private routerChangeService: RouterChangeService,
              private auth: LocalStorageService) {
    this.routerChangeService.activeRoute.subscribe((e) => {
      this.activeUrl = e.url;
    });
    this.userFirstName = this.auth.getValue('firstName', false);
    this.userLastName = this.auth.getValue('lastName', false);
  }
  ngOnInit(): void {

  }
  logout() {
    this.auth.remove(environment.authKey);
    this.auth.remove('firstName');
    this.auth.remove('lastName');
    this.auth.remove('email');
    this.auth.remove('phone');
    this.auth.remove('username');    window.location.reload();
  }
}
