import { Component, OnInit, ViewChild } from '@angular/core';
import {ApiService} from '../../../../../core/http/api.service';
import {environment} from '../../../../../../environments/environment';
import { CalendarComponent, FocusEventArgs } from '@syncfusion/ej2-angular-calendars';
import {SnotifyService} from 'ng-snotify';
import { Router } from '@angular/router';


declare var $: any;
@Component({
  selector: 'app-class-landing',
  templateUrl: './class-landing.component.html',
  styleUrls: ['./class-landing.component.css']
})
export class ClassLandingComponent implements OnInit {
  classList = [];
  classid: any;
  assetsUrl = environment.assetsUrl;
  availability: any;
  value: Date;
  public datepickerObj: CalendarComponent;
  constructor(private api: ApiService, private snotify: SnotifyService, private router: Router ) {
    $('html, body').animate(
      { scrollTop: '0' }, 800);
    
    //this.value = new Date();
  }
  ngOnInit(): void {
    this.getClassList();
    //this.getAvailability();
  }
  getClassList() {
    this.api.getClassListByTeacher().subscribe( res => {
      this.classList = res.response;
      this.classList.forEach((ele) => {if(ele.class_duration_time == 1) ele.class_duration_time=30; else if(ele.class_duration_time == 2) ele.class_duration_time=45; else ele.class_duration_time=60;});
      //console.log(this.classList , "here");
    });
  }

  onRenderCell(args) {
    /*Apply selected format to the component*/
    // if (args.date.getDay() == 0 || args.date.getDay() == 6) {
    //   //sets isDisabled to true to disable the date.
    //   args.isDisabled = true;
    //   console.log(args);
    // }

    //console.log(args.date.getHours());
  }

  onFocus(args: FocusEventArgs): void {       // can change the properties of date pickers from here...
    //console.log(args.model["properties"]["openOnFocus"]);
    args.model["properties"]["openOnFocus"]=true;
  }

  getAvailability() {
    this.api.getAvailability().subscribe(res => {
      this.availability = res.response;
    });
  }

  previewClass(id) {
    window.open('preview/' + id, '_blank');
  }

  sendId(data) {
    console.log(data);
    this.classid = data;
  }

  // checkMonday(i, value) {
  //   if (value.checked == true) {
  //     value = 1;
  //     this.availability[i].monday = value;
  //   } else {
  //     value = 0;
  //     this.availability[i].monday = value;
  //   }
  // }

  // checkTuesday(i, value) {
  //   if (value.checked == true) {
  //     value = 1;
  //     this.availability[i].tuesday = value;
  //   } else {
  //     value = 0;
  //     this.availability[i].tuesday = value;
  //   }
  // }

  // checkWednesday(i, value) {
  //   if (value.checked == true) {
  //     value = 1;
  //     this.availability[i].wednesday = value;
  //   } else {
  //     value = 0;
  //     this.availability[i].wednesday = value;
  //   }
  // }

  // checkThursday(i, value) {
  //   if (value.checked == true) {
  //     value = 1;
  //     this.availability[i].thursday = value;
  //   } else {
  //     value = 0;
  //     this.availability[i].thursday = value;
  //   }
  // }

  // checkFriday(i, value) {
  //   console.log(value.checked);
  //   if (value.checked == true) {
  //     value = 1;
  //     this.availability[i].friday = value;
  //   } else {
  //     value = 0;
  //     this.availability[i].friday = value;
  //   }
  // }

  // checkSaturday(i, value) {
  //   if (value.checked == true) {
  //     value = 1;
  //     this.availability[i].saturday = value;
  //   } else {
  //     value = 0;
  //     this.availability[i].saturday = value;
  //   }
  // }

  // checkSunday(i, value) {
  //   if (value.checked == true) {
  //     value = 1;
  //     this.availability[i].sunday = value;
  //   } else {
  //     value = 0;
  //     this.availability[i].sunday = value;
  //   }
  // }

  // SelectAll() {
  //   for (let i = 0; i < this.availability.length; i++) {
  //     this.availability[i].monday = 1;
  //     this.availability[i].tuesday = 1;
  //     this.availability[i].wednesday = 1;
  //     this.availability[i].thursday = 1;
  //     this.availability[i].friday = 1;
  //     this.availability[i].saturday = 1;
  //     this.availability[i].sunday = 1;
  //   }
  // }

  // DeselectAll() {
  //   for (let i = 0; i < this.availability.length; i++) {
  //     this.availability[i].monday = 0;
  //     this.availability[i].tuesday = 0;
  //     this.availability[i].wednesday = 0;
  //     this.availability[i].thursday = 0;
  //     this.availability[i].friday = 0;
  //     this.availability[i].saturday = 0;
  //     this.availability[i].sunday = 0;
  //   }
  // }

  sendAvailability() {
    //this.api.sendAvailability({slots: this.availability}).subscribe(res => {
      //this.value.setDate(this.value.getDate() + 1);
    this.api.schedulingclass(this.classid , this.value).subscribe(
      res => {
        console.log(res);
        $('#staticBackdrop').modal('hide');
        this.snotify.success(res.message);
        setTimeout(()=> {this.router.navigate(['./teacher/schedule/calander']);}, 2000 ) ;
      },
      (error) => {
        if (error.error.message) {
          this.snotify.error(error.error.message, 'Error');
          //this.value.setDate(this.value.getDate() - 1);
        } else {
          this.snotify.error('Please try again later, Server didn\'t respond', 'Error');
          //this.value.setDate(this.value.getDate() - 1);
        }
      }
    );
  }
      
}
