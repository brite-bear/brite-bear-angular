import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseFormatComponent } from './course-format.component';

describe('CourseFormatComponent', () => {
  let component: CourseFormatComponent;
  let fixture: ComponentFixture<CourseFormatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseFormatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseFormatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
