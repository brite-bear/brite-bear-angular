import {Component, OnInit, NgZone} from '@angular/core';
import {ApiService} from '../../../../../core/http/api.service';
import {LocalStorageService} from '../../../../../core/services/local-storage.service';
import {SnotifyService} from 'ng-snotify';
import {ActivatedRoute, Router} from '@angular/router';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {FileSystemDirectoryEntry, FileSystemFileEntry, NgxFileDropEntry} from 'ngx-file-drop';
import {environment} from '../../../../../../environments/environment';

declare var $: any;

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.css']
})
export class ResourcesComponent implements OnInit {

  constructor(private apiServices: ApiService,
              private auth: LocalStorageService,
              private snotifyService: SnotifyService,
              private router: Router, private zone: NgZone, private route: ActivatedRoute) {
    $('html, body').animate(
      {scrollTop: '350'}, 800);
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });
    // this.locallySavedID = this.auth.getValue('classID', false);
  }

  assetsUrl = environment.assetsUrl;
  loaderMaterial = false;
  loaderAssessments = false;
  business = 1;
  id;
  classObj: any = {
    id: '',
    title: '',
    summary: '',
    subject_id: '',
    class_type_id: '',
    from_age: 0,
    to_age: 0,
    size_min: 0,
    size_max: 0,
    class_duration: '',
    class_duration_type: '',
    class_duration_time: '',
    cover_image: '',
    cover_video: '',
    description: '',
    assignment: '',
    goal: '',
    assessment: '',
    hours_per_week: 0,
    parent_guidance: '',
    sources: '',
    teacher_expertise: '',
    external_resources: [],
    class_material_title: '',
    class_material_attachments: [],
    assignment_title: '',
    assignment_attachments: [],
    price_per_student: 0,
    welcome_note: '',
    welcome_cover_path: ''
  };
  savedClassData;
  getSavedClass;
  // locallySavedID;
  externalName;
  externalUrl;
  classMaterialEvent;
  materialImages = [];
  blankUpload = '';
  classAssessmentsEvent;
  assessmentImages = [];
  blankAssessment = '';

  imageChangedEvent: any = '';
  croppedImage: any = '';
  showCropper = false;
  allUploadedFiles = [];
  allUploadedAsses = [];

  public files: NgxFileDropEntry[] = [];
  public assessmentFiles: NgxFileDropEntry[] = [];

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    for (const droppedFile of files) {

      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {

          // Here you can access the real file
          this.uploadImage(file);
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  public drop(assessFile: NgxFileDropEntry[]) {
    this.assessmentFiles = assessFile;
    for (const assessmentDropFile of assessFile) {

      // Is it a file?
      if (assessmentDropFile.fileEntry.isFile) {
        const fileAssessEntry = assessmentDropFile.fileEntry as FileSystemFileEntry;
        fileAssessEntry.file((assFile: File) => {

          // Here you can access the real file
          this.uploadAssessments(assFile);
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileAssessEntry = assessmentDropFile.fileEntry as FileSystemDirectoryEntry;
        console.log(assessmentDropFile.relativePath, fileAssessEntry);
      }
    }
  }

  public fileOver(event) {
    console.log(event);
  }

  public fileOverAssess(event) {
    console.log(event);
  }

  public fileLeave(event) {
    console.log(event);
  }

  public fileLeaveAssess(event) {
    console.log(event);
  }

  ngOnInit(): void {
    if (this.id) {
      this.getClass();
    }
  }

  uploadImage(file) {
    
    this.apiServices.uploadClassFiles(file).subscribe(res => {
      this.zone.run(() => {
        this.materialImages.push({name: file.name, type: file.type});
        this.blankUpload = '';
      });
      this.auth.setValue('materialAttach', this.materialImages);
      this.snotifyService.success('Material successfully uploaded', 'Success');
    }, (error) => {
      this.snotifyService.error(error.error.message, 'Error');
    });
    
  }

  uploadAssessments(file) {
    
    this.apiServices.uploadClassFiles(file).subscribe(res => {
      this.zone.run(() => {
        this.assessmentImages.push(res.response);
        this.blankAssessment = '';
      });
      this.auth.setValue('assessmentAttach', this.assessmentImages);
      this.snotifyService.success('Assessment successfully uploaded', 'Success');
    }, (error) => {
      this.snotifyService.error(error.error.message, 'Error');
    });
    
  }

  getClass() {
    this.apiServices.getAddedClass(this.id).subscribe(res => {
      this.getSavedClass = res.response;
      this.classObj.id = this.getSavedClass.id;
      this.classObj.title = this.getSavedClass.title;
      this.classObj.summary = this.getSavedClass.summary;
      this.classObj.subject_id = this.getSavedClass.subject;
      this.classObj.class_type_id = this.getSavedClass.classType.id;
      this.classObj.from_age = this.getSavedClass.fromAge;
      this.classObj.to_age = this.getSavedClass.toAge;
      this.classObj.size_min = this.getSavedClass.sizeMin;
      this.classObj.size_max = this.getSavedClass.sizeMax;
      this.classObj.class_duration = this.getSavedClass.classDuration;
      this.classObj.class_duration_type = this.getSavedClass.classDurationType;
      this.classObj.class_duration_time = this.getSavedClass.classDurationTime;
      this.classObj.cover_image = this.getSavedClass.coverImage;
      this.classObj.cover_video = this.getSavedClass.coverVideo;
      this.classObj.description = this.getSavedClass.description;
      this.classObj.assignment = this.getSavedClass.assignment;
      this.classObj.goal = this.getSavedClass.goal;
      this.classObj.assessment = this.getSavedClass.assessment;
      this.classObj.hours_per_week = this.getSavedClass.hoursPerWeek;
      this.classObj.parent_guidance = this.getSavedClass.parentGuidance;
      this.classObj.sources = this.getSavedClass.sources;
      this.classObj.teacher_expertise = this.getSavedClass.teacherExpertise;
      this.classObj.external_resources = this.getSavedClass.externalResource;
      
      
      if (this.getSavedClass.classMaterial.length > 0) {
        this.classObj.class_material_title = this.getSavedClass.classMaterial[0].information;
      }
      
      this.classObj.class_material_attachments = this.getSavedClass.classMaterialAttachment;
      
      if (this.getSavedClass.classAssignment.length > 0) {
        this.classObj.assignment_title = this.getSavedClass.classAssignment[0].information;
      }
      
      this.classObj.assignment_attachments = this.getSavedClass.classAssingmentAttachment;
      this.classObj.price_per_student = this.getSavedClass.pricePerStudent;
      this.classObj.welcome_note = this.getSavedClass.welcomeNote;
      this.classObj.welcome_cover_path = this.getSavedClass.welcomeCoverImage;
      

    });
  }

  SaveClassDetailsAsDraft() {
    this.classObj.class_material_attachments = [];
    for (let i = 0; i < this.allUploadedFiles.length; i++) {
      this.classObj.class_material_attachments.push(this.allUploadedFiles[i]);
    }
    
    this.classObj.assignment_attachments = [];
    for (let i = 0; i < this.allUploadedAsses.length; i++) {
      this.classObj.assignment_attachments.push(this.allUploadedAsses[i]);
    }
    if (!this.id) {
      this.apiServices.AddClass(this.classObj).subscribe(res => {
        this.savedClassData = res.response;
        this.snotifyService.success('Saved As Draft Successfully');
        this.classObj.id = this.savedClassData.id;
        this.auth.setValue('classID', this.classObj.id);
      });
    }
    if (this.id) {
      this.apiServices.AddClass(this.classObj).subscribe(res => {
        this.savedClassData = res.response;
        this.snotifyService.success('Saved As Draft Successfully');
      });
    }
  }

  nextAndSave() {
    
    this.classObj.class_material_attachments = [];
    for (let i = 0; i < this.allUploadedFiles.length; i++) {
      this.classObj.class_material_attachments.push(this.allUploadedFiles[i]);
    }
    this.classObj.assignment_attachments = [];
    for (let i = 0; i < this.allUploadedAsses.length; i++) {
      this.classObj.assignment_attachments.push(this.allUploadedAsses[i]);
    }
    
    if (!this.id) {
      this.apiServices.AddClass(this.classObj).subscribe(res => {
        this.savedClassData = res.response;
        this.classObj.id = this.savedClassData.id;
        this.auth.setValue('classID', this.classObj.id);
        this.router.navigate(['teacher/class/pricing/' + this.id]);
      });
    }
    if (this.id) {
      this.apiServices.AddClass(this.classObj).subscribe(res => {
        this.savedClassData = res.response;
        this.router.navigate(['teacher/class/pricing/' + this.id]);
      });
    }
  }

  addMoreResource() {
    if (this.externalUrl && this.externalUrl) {
      this.classObj.external_resources.push({name: this.externalName, url: this.externalUrl});
      this.externalName = '';
      this.externalUrl = '';
    } else {
      this.snotifyService.warning('Please fill both the fields to add more');
    }
  }

  deleteAddedResource(i) {
    this.classObj.external_resources.splice(i, 1);
  }

  deleteAddedMaterial(i) {
    this.materialImages.splice(i, 1);
  }

  deleteAddedAssessments(i) {
    this.assessmentImages.splice(i, 1);
  }

  uploadClassMaterial(file) {
    this.loaderMaterial = true;
 
    this.classMaterialEvent = file.target.files[0];
    this.apiServices.uploadClassFiles(this.classMaterialEvent).subscribe(res => {
      this.zone.run(() => {
        
        // this.allUploadedFiles.push({path: res.response, type: file.target.files[0].type, name: file.target.files[0].name});
        this.allUploadedFiles.push(res.response);
        this.materialImages.push({name: file.target.files[0].name, type: file.target.files[0].type});
        this.blankUpload = '';
      });
      this.auth.setValue('materialAttach', this.materialImages);
      this.snotifyService.success('Class Material Added Successfully', 'Success');
      this.loaderMaterial = false;
    }, (error) => {
      this.snotifyService.error(error.error.message, 'Error');
      this.loaderMaterial = false;
    });
  }

  uploadClassAssessments(file) {
    this.loaderAssessments = true;
    this.classAssessmentsEvent = file.target.files[0];
    this.apiServices.uploadClassFiles(this.classAssessmentsEvent).subscribe(res => {
      this.zone.run(() => {
        this.allUploadedAsses.push(res.response);
        this.assessmentImages.push({name: file.target.files[0].name, type: file.target.files[0].type});
        this.blankAssessment = '';
      });
      this.auth.setValue('assessmentAttach', this.assessmentImages);
      this.snotifyService.success('Class assessment successfully uploaded', 'Success');
      this.loaderAssessments = false;
    }, (error) => {
      this.snotifyService.error(error.error.message, 'Error');
      this.loaderAssessments = false;
    });
  }
}
