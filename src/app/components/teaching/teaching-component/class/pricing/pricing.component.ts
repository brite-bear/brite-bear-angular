import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../../../../core/http/api.service';
import {LocalStorageService} from '../../../../../core/services/local-storage.service';
import {SnotifyService} from 'ng-snotify';
import {ActivatedRoute, Router} from '@angular/router';
import {Options} from 'ng5-slider';
import {min} from 'rxjs/operators';

declare var $: any;
@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.css']
})
export class PricingComponent implements OnInit {
  loader = false;
  loaderDraft = false;
  locallySavedClassType;
  classObj = {
    id: '',
    title: '',
    summary: '',
    subject_id: '',
    class_type_id: '',
    from_age: 0,
    to_age: 0,
    size_min: 0,
    size_max: 0,
    class_duration: '',
    class_duration_type: '',
    class_duration_time: '',
    cover_image: '',
    cover_video: '',
    description: '',
    assignment: '',
    goal: '',
    assessment: '',
    hours_per_week: 0,
    parent_guidance: '',
    sources: '',
    teacher_expertise: '',
    external_resources: [],
    class_material_title: '',
    class_material_attachments: [],
    assignment_title: '',
    assignment_attachments: [],
    price_per_student: 0,
    welcome_note: '',
    welcome_cover_path: ''
  };
  savedClassData;
  getSavedClass;
  // locallySavedID;
  materialAttach;
  options: Options = {
    floor: 0,
    ceil: 50,
    step: 0.5,
    showTicks: true,
    animate: true
  };
  minRangeValue;
  maxRangeValue;
  id;
  selectedWeek;
  constructor(private apiServices: ApiService,
              private auth: LocalStorageService,
              private snotifyService: SnotifyService,
              private router: Router, private route: ActivatedRoute) {
    $('html, body').animate(
      {scrollTop: '350'}, 800);
    // this.locallySavedID = this.auth.getValue('classID', false);
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });
    this.materialAttach = this.auth.getValue('materialAttach', true);
    this.locallySavedClassType = this.auth.getValue('classType', false);
    this.selectedWeek = this.auth.getValue('NoOfWeeks', false);
  }

  ngOnInit(): void {
    if (this.id) {
      this.getClass();
    }
  }

  getClass() {
    this.loader = true;
    this.loaderDraft = true;
    this.apiServices.getAddedClass(this.id).subscribe(res => {
      this.loader = false;
      this.loaderDraft = false;
      this.getSavedClass = res.response;
      this.classObj.id = this.getSavedClass.id;
      this.classObj.title = this.getSavedClass.title;
      this.classObj.summary = this.getSavedClass.summary;
      this.classObj.subject_id = this.getSavedClass.subject;
      this.classObj.class_type_id = this.getSavedClass.classType.id;
      this.classObj.from_age = this.getSavedClass.fromAge;
      this.classObj.to_age = this.getSavedClass.toAge;
      this.classObj.size_min = this.getSavedClass.sizeMin;
      this.classObj.size_max = this.getSavedClass.sizeMax;
      this.classObj.class_duration = this.getSavedClass.classDuration;
      this.classObj.class_duration_type = this.getSavedClass.classDurationType;
      this.classObj.class_duration_time = this.getSavedClass.classDurationTime;
      this.classObj.cover_image = this.getSavedClass.coverImage;
      this.classObj.cover_video = this.getSavedClass.coverVideo;
      this.classObj.description = this.getSavedClass.description;
      this.classObj.assignment = this.getSavedClass.assignment;
      this.classObj.goal = this.getSavedClass.goal;
      this.classObj.assessment = this.getSavedClass.assessment;
      this.classObj.hours_per_week = this.getSavedClass.hoursPerWeek;
      this.classObj.parent_guidance = this.getSavedClass.parentGuidance;
      this.classObj.sources = this.getSavedClass.sources;
      this.classObj.teacher_expertise = this.getSavedClass.teacherExpertise;
      this.classObj.external_resources = this.getSavedClass.externalResource;
      if (this.getSavedClass.classMaterial.length > 0) {
        this.classObj.class_material_title = this.getSavedClass.classMaterial[0].information;
      }
      this.classObj.class_material_attachments = this.getSavedClass.classMaterialAttachment;
      if (this.getSavedClass.classAssignment.length > 0) {
        this.classObj.assignment_title = this.getSavedClass.classAssignment[0].information;
      }
      this.classObj.assignment_attachments = this.getSavedClass.classAssingmentAttachment;
      this.classObj.price_per_student = this.getSavedClass.pricePerStudent;
      this.classObj.welcome_note = this.getSavedClass.welcomeNote;
      this.classObj.welcome_cover_path = this.getSavedClass.welcomeCoverImage;
      this.calculatePricingFormula();
    });
  }

  SaveClassDetailsAsDraft() {
    this.loaderDraft = true;
    if (!this.id) {
      this.apiServices.AddClass(this.classObj).subscribe(res => {
        this.loaderDraft = false;
        this.savedClassData = res.response;
        this.snotifyService.success('Saved As Draft Successfully');
        this.classObj.id = this.savedClassData.id;
        this.auth.setValue('classID', this.classObj.id);
      });
    }
    if (this.id) {
      this.apiServices.AddClass(this.classObj).subscribe(res => {
        this.loaderDraft = false;
        this.savedClassData = res.response;
        this.snotifyService.success('Saved As Draft Successfully');
      });
    }
  }
  con() {
    // console.log(this.classObj.price_per_student);
    const minCommission = (this.classObj.size_min * this.classObj.price_per_student);
    const maxCommission = (this.classObj.size_max * this.classObj.price_per_student);
    console.log(this.classObj.size_min);
    console.log(this.classObj.size_max);
    console.log(minCommission);
    console.log(maxCommission);
    if (this.locallySavedClassType == 'MULTI-DAY') {
      console.log(this.locallySavedClassType);
      console.log(this.selectedWeek);
      this.minRangeValue = (minCommission - ((30 * minCommission) / 100)) / this.selectedWeek;
      this.maxRangeValue = (maxCommission - ((30 * maxCommission) / 100)) / this.selectedWeek;
    }
    if (this.locallySavedClassType == 'ONE-TIME' || this.locallySavedClassType === 'ONGOING') {
      this.minRangeValue = (minCommission - ((30 * minCommission) / 100));
      this.maxRangeValue = (maxCommission - ((30 * maxCommission) / 100));
    }
    // console.log(this.minRangeValue);
    // console.log(this.maxRangeValue);
  }

  nextAndSave() {
    if (this.classObj.price_per_student) {
    this.loader = true;
    if (!this.id) {
      this.apiServices.AddClass(this.classObj).subscribe(res => {
        this.loader = false;
        this.savedClassData = res.response;
        this.classObj.id = this.savedClassData.id;
        this.auth.setValue('classID', this.classObj.id);
        this.router.navigate(['teacher/class/welcome/' + this.id]);
      });
    }
    if (this.id) {
      this.apiServices.AddClass(this.classObj).subscribe(res => {
        this.loader = false;
        this.savedClassData = res.response;
        this.router.navigate(['teacher/class/welcome/' + this.id]);
      });
    }
  } else {
      this.snotifyService.warning('Please estimate before continuing');
      this.loader = false;
    }
  }
  calculatePricingFormula() {
    const minCommission = (this.classObj.size_min * this.classObj.price_per_student);
    const maxCommission = (this.classObj.size_max * this.classObj.price_per_student);
    /*console.log(this.classObj.size_min);
    console.log(this.classObj.size_max);
    console.log(minCommission);
    console.log(maxCommission);*/
    if (this.locallySavedClassType === 'MULTI-DAY') {
      // console.log(this.locallySavedClassType);
      this.minRangeValue = (minCommission - ((30 * minCommission) / 100)) / this.selectedWeek;
      this.maxRangeValue = (maxCommission - ((30 * maxCommission) / 100)) / this.selectedWeek;
    }
    if (this.locallySavedClassType === 'ONE-TIME' || this.locallySavedClassType === 'ONGOING' ) {
      this.minRangeValue = (minCommission - ((30 * minCommission) / 100));
      this.maxRangeValue = (maxCommission - ((30 * maxCommission) / 100));
    }
    // console.log(this.minRangeValue);
    // console.log(this.maxRangeValue);
  }
}
