import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../../../core/http/api.service';
import {LocalStorageService} from '../../../../../core/services/local-storage.service';
import {SnotifyService} from 'ng-snotify';
import {ActivatedRoute, Router} from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-class-details',
  templateUrl: './class-details.component.html',
  styleUrls: ['./class-details.component.css']
})
export class ClassDetailsComponent implements OnInit {
  loader = false;
  loaderDraft = false;
classObj = {
  id: '',
  title: '',
  summary: '',
  subject_id: '',
  class_type_id: '',
  from_age: 0,
  to_age: 0,
  size_min: 0,
  size_max: 0,
  class_duration: '',
  class_duration_type: '',
  class_duration_time: '',
  cover_image: '',
  cover_video: '',
  description: '',
  assignment: '',
  goal: '',
  assessment: '',
  hours_per_week: 0,
  parent_guidance: '',
  sources: '',
  teacher_expertise: '',
  external_resources: [],
  class_material_title: '',
  class_material_attachments: [],
  assignment_title: '',
  assignment_attachments: [],
  price_per_student: 0,
  welcome_note: '',
  welcome_cover_path: ''
};
allSubjects = [];
allClassTypes = [];
savedClassData;
  getSavedClass;
  // locallySavedID;
  locallySavedClassType;
  id;
  constructor(private apiServices: ApiService,
              private auth: LocalStorageService,
              private snotifyService: SnotifyService,
              private router: Router, private route: ActivatedRoute) {
    $('html, body').animate(
      { scrollTop: '350' }, 800);
    $('.selectpicker').selectpicker('refresh');
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });
    // this.locallySavedID = this.auth.getValue('classID', false);
    this.locallySavedClassType = this.auth.getValue('classType', false);
  }
  ngOnInit(): void {
    this.getAllSubjects();
    this.getClassTypes();
    if (this.id) {
      this.getClass();
    }
  }

  getAllSubjects() {
    $('.selectpicker').selectpicker('refresh');
    this.apiServices.getSubjects().subscribe( res => {
      this.allSubjects = res.response;
    });
  }
  getClassTypes() {
    this.apiServices.getClassTypes().subscribe( res => {
      this.allClassTypes = res.response;
    });
  }
  CheckType(type) {
    console.log(type);
    this.auth.setValue('classTypeID', this.classObj.class_type_id);
    this.auth.setValue('classType', type);
    this.locallySavedClassType = type;
  }
  SaveClassDetailsAsDraft() {
    this.loaderDraft = true;
    if (!this.id) {
      this.apiServices.AddClass(this.classObj).subscribe( res => {
        this.loaderDraft = false;
        this.savedClassData = res.response;
        this.snotifyService.success('Saved As Draft Successfully');
        this.classObj.id = this.savedClassData.id;
        this.id=this.savedClassData.id;
        this.auth.setValue('classID', this.classObj.id);
      });
    }
    else if (this.id) {
      this.apiServices.AddClass(this.classObj).subscribe( res => {
        this.loaderDraft = false;
        this.savedClassData = res.response;
        this.snotifyService.success('Saved As Draft Successfully');
      });
    }
  }
  checkSubject(id) {
    for (let i = 0; i < this.allSubjects.length; i++) {
      if (id == this.allSubjects[i].id) {
          console.log(this.allSubjects[i].subject);
          this.auth.setValue('selectedSubject', this.allSubjects[i].subject);
        }
      }
  }
  getClass() {
    this.loader = true;
    this.apiServices.getAddedClass(this.id).subscribe( res => {
      this.loader = false;
      this.getSavedClass = res.response;
      this.classObj.id = this.getSavedClass.id;
      this.classObj.title = this.getSavedClass.title;
      this.classObj.summary = this.getSavedClass.summary;
      this.classObj.subject_id = this.getSavedClass.subject;
      this.classObj.class_type_id = this.getSavedClass.classType.id;
      this.classObj.from_age = this.getSavedClass.fromAge;
      this.classObj.to_age = this.getSavedClass.toAge;
      this.classObj.size_min = this.getSavedClass.sizeMin;
      this.classObj.size_max = this.getSavedClass.sizeMax;
      this.classObj.class_duration = this.getSavedClass.classDuration;
      this.classObj.class_duration_type = this.getSavedClass.classDurationType;
      this.classObj.class_duration_time = this.getSavedClass.classDurationTime;
      this.classObj.cover_image = this.getSavedClass.coverImage;
      this.classObj.cover_video = this.getSavedClass.coverVideo;
      this.classObj.description = this.getSavedClass.description;
      this.classObj.assignment = this.getSavedClass.assignment;
      this.classObj.goal = this.getSavedClass.goal;
      this.classObj.assessment = this.getSavedClass.assessment;
      this.classObj.hours_per_week = this.getSavedClass.hoursPerWeek;
      this.classObj.parent_guidance = this.getSavedClass.parentGuidance;
      this.classObj.sources = this.getSavedClass.sources;
      this.classObj.teacher_expertise = this.getSavedClass.teacherExpertise;
      this.classObj.external_resources = this.getSavedClass.externalResource;
      if (this.getSavedClass.classMaterial.length > 0) {
        this.classObj.class_material_title = this.getSavedClass.classMaterial[0].information;
      }
      this.classObj.class_material_attachments = this.getSavedClass.classMaterialAttachment;
      if (this.getSavedClass.classAssignment.length > 0) {
        this.classObj.assignment_title = this.getSavedClass.classAssignment[0].information;
      }
      this.classObj.assignment_attachments = this.getSavedClass.classAssingmentAttachment;
      this.classObj.price_per_student = this.getSavedClass.pricePerStudent;
      this.classObj.welcome_note = this.getSavedClass.welcomeNote;
      this.classObj.welcome_cover_path = this.getSavedClass.welcomeCoverImage;
      this.checkSubject(this.classObj.subject_id);
      this.CheckType(this.locallySavedClassType);
    });
  }
  nextAndSave() {
    this.loader = true;
    if (this.classObj.summary) {

    if (!this.classObj.id) {
      
      this.apiServices.AddClass(this.classObj).subscribe( res => {
        this.loader = false;
        this.savedClassData = res.response;
        this.classObj.id = this.savedClassData.id;
        this.auth.setValue('classID', this.classObj.id);
        this.router.navigate(['teacher/class/course/' + this.classObj.id]);
      });
    }
    if (this.classObj.id) {
      
      this.apiServices.AddClass(this.classObj).subscribe( res => {
        this.loader = false;
        this.savedClassData = res.response;
        this.router.navigate(['teacher/class/course/' + this.classObj.id]);
      });
    }
  } else {
      this.snotifyService.warning('Please Enter summary of your subject');
      this.loader = false;
    }
  }
}
