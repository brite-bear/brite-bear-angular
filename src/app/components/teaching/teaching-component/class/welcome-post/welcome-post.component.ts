import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../../../core/http/api.service';
import {LocalStorageService} from '../../../../../core/services/local-storage.service';
import {SnotifyService} from 'ng-snotify';
import {ActivatedRoute, Router} from '@angular/router';
import {Dimensions, ImageCroppedEvent} from 'ngx-image-cropper';
import {environment} from '../../../../../../environments/environment';
// import { Dimensions, ImageCroppedEvent, ImageTransform } from './image-cropper/interfaces/index';
// import {base64ToFile} from './image-cropper/utils/blob.utils';
declare var $: any;
@Component({
  selector: 'app-welcome-post',
  templateUrl: './welcome-post.component.html',
  styleUrls: ['./welcome-post.component.css']
})
export class WelcomePostComponent implements OnInit {
  assetsUrl = environment.assetsUrl;
  classObj: any = {
    id: '',
    title: '',
    summary: '',
    subject_id: '',
    class_type_id: '',
    from_age: 0,
    to_age: 0,
    size_min: 0,
    size_max: 0,
    class_duration: '',
    class_duration_type: '',
    class_duration_time: '',
    cover_image: '',
    cover_video: '',
    description: '',
    assignment: '',
    goal: '',
    assessment: '',
    hours_per_week: 0,
    parent_guidance: '',
    sources: '',
    teacher_expertise: '',
    external_resources: [],
    class_material_title: '',
    class_material_attachments: [],
    assignment_title: '',
    assignment_attachments: [],
    price_per_student: 0,
    welcome_note: '',
    welcome_cover_path: ''
  };
  savedClassData;
  getSavedClass;
  // locallySavedID;
  loaderWelcome = false;
  // welcomePostEvent: any = '';
  // image;

  imageChangedEvent: any = '';
  croppedImage: any = '';
  showCropper = false;
  image;
  FileType;
  id;

  constructor(private apiServices: ApiService,
              private auth: LocalStorageService,
              private snotifyService: SnotifyService,
              private router: Router, private route: ActivatedRoute) {
    $('html, body').animate(
      {scrollTop: '350'}, 800);
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });
    // this.locallySavedID = this.auth.getValue('classID', false);
  }

  ngOnInit(): void {
    if (this.id) {
      this.getClass();
    }
  }

  getClass() {
    this.apiServices.getAddedClass(this.id).subscribe(res => {
      this.getSavedClass = res.response;
      this.classObj.id = this.getSavedClass.id;
      this.classObj.title = this.getSavedClass.title;
      this.classObj.summary = this.getSavedClass.summary;
      this.classObj.subject_id = this.getSavedClass.subject;
      this.classObj.class_type_id = this.getSavedClass.classType.id;
      this.classObj.from_age = this.getSavedClass.fromAge;
      this.classObj.to_age = this.getSavedClass.toAge;
      this.classObj.size_min = this.getSavedClass.sizeMin;
      this.classObj.size_max = this.getSavedClass.sizeMax;
      this.classObj.class_duration = this.getSavedClass.classDuration;
      this.classObj.class_duration_type = this.getSavedClass.classDurationType;
      this.classObj.class_duration_time = this.getSavedClass.classDurationTime;
      this.classObj.cover_image = this.getSavedClass.coverImage;
      this.classObj.cover_video = this.getSavedClass.coverVideo;
      this.classObj.description = this.getSavedClass.description;
      this.classObj.assignment = this.getSavedClass.assignment;
      this.classObj.goal = this.getSavedClass.goal;
      this.classObj.assessment = this.getSavedClass.assessment;
      this.classObj.hours_per_week = this.getSavedClass.hoursPerWeek;
      this.classObj.parent_guidance = this.getSavedClass.parentGuidance;
      this.classObj.sources = this.getSavedClass.sources;
      this.classObj.teacher_expertise = this.getSavedClass.teacherExpertise;
      this.classObj.external_resources = this.getSavedClass.externalResource;
      if (this.getSavedClass.classMaterial.length > 0) {
        this.classObj.class_material_title = this.getSavedClass.classMaterial[0].information;
      }
      this.classObj.class_material_attachments = this.getSavedClass.classMaterialAttachment;
      if (this.getSavedClass.classAssignment.length > 0) {
        this.classObj.assignment_title = this.getSavedClass.classAssignment[0].information;
      }
      this.classObj.assignment_attachments = this.getSavedClass.classAssingmentAttachment;
      this.classObj.price_per_student = this.getSavedClass.pricePerStudent;
      this.classObj.welcome_note = this.getSavedClass.welcomeNote;
      this.classObj.welcome_cover_path = this.getSavedClass.welcomeCoverImage;
    });
  }

  SaveClassDetailsAsDraft() {
    if (!this.id) {
      this.apiServices.AddClass(this.classObj).subscribe(res => {
        this.savedClassData = res.response;
        this.snotifyService.success('Saved As Draft Successfully');
        this.classObj.id = this.savedClassData.id;
        this.auth.setValue('classID', this.classObj.id);
      });
    }
    if (this.id) {
      this.apiServices.AddClass(this.classObj).subscribe(res => {
        this.savedClassData = res.response;
        this.snotifyService.success('Saved As Draft Successfully');
      });
    }
  }

  nextAndSave() {
    if (this.classObj.welcome_cover_path && this.classObj.welcome_note) {
      if (!this.id) {
        this.apiServices.AddClass(this.classObj).subscribe(res => {
          this.savedClassData = res.response;
          this.classObj.id = this.savedClassData.id;
          this.requestListing();
          this.snotifyService.success('Class Created Successfully', 'Success');
        });
      }
      if (this.id) {
        this.apiServices.AddClass(this.classObj).subscribe(res => {
          this.savedClassData = res.response;
          this.requestListing();
          this.snotifyService.success('Class Created Successfully', 'Success');
        });
      }
    } else {
      this.snotifyService.warning('Please Upload your welcome post with short note to request your listing');
    }
  }

  requestListing() {
    this.apiServices.requestListing({id: this.id}).subscribe(res => {
      
      this.auth.remove('classID');
      this.router.navigate(['teacher/class']);
    });
  }

  uploadWelcomePost(file) {
    this.loaderWelcome = true;
    this.croppedImage=file.target.files[0];
    this.apiServices.uploadCroppedImage(this.croppedImage).subscribe(res => {
      this.classObj.welcome_cover_path = res.response;
      $('#cropImageModal').modal('hide');
      this.snotifyService.success('Welcome post successfully uploaded', 'Success');
      this.loaderWelcome = false;
    }, (error) => {
      this.snotifyService.error(error.error.message, 'Error');
      $('#cropImageModal').modal('hide');
      this.loaderWelcome = false;
    });
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    
  }

  /* fileChangeEvent(event: any): void {
     $('#cropImageModal').modal('show');
     this.imageChangedEvent = event;
   }*/
  fileChangeEvent(event: any) {
    this.FileType = event.target.files[0].type;
    
    if (this.FileType !== 'image/png' && this.FileType !== 'image/jpeg') {
      this.snotifyService.error('Please select .png or jpg or jpeg');
      $('#cropImageModal').modal('hide');
    } else {
      $('#cropImageModal').modal('show');
      this.imageChangedEvent = event;
    }
  }

  imageLoaded() {
    this.showCropper = true;
    console.log('Image loaded');
  }

  cropperReady() {
  }

  loadImageFailed() {
    console.log('Load failed');
  }

  showWelcomePreview() {
    console.log(this.classObj.welcome_cover_path);
    console.log(this.classObj.welcome_note);
    if (!this.classObj.welcome_cover_path) {
      this.snotifyService.warning('Please Upload Background Cover Image before proceeding');
    } else if (!this.classObj.welcome_note) {
      this.snotifyService.warning('Please Enter Welcome Note To Preview');
    } else if (this.classObj.welcome_note && this.classObj.welcome_cover_path) {
      $('#welcomePost').modal('show');
    }
  }

  previewClass() {
    if (this.classObj.welcome_note && this.classObj.welcome_cover_path) {
      window.open('preview/' + this.id, '_blank');
    } else {
      this.snotifyService.warning('Please Upload your welcome post with short note to preview your class');
    }
  }
}
