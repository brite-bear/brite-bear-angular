import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WelcomePostComponent } from './welcome-post.component';

describe('WelcomePostComponent', () => {
  let component: WelcomePostComponent;
  let fixture: ComponentFixture<WelcomePostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WelcomePostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomePostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
