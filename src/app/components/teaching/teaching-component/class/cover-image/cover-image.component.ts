import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../../../core/http/api.service';
import {LocalStorageService} from '../../../../../core/services/local-storage.service';
import {SnotifyService} from 'ng-snotify';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../../../../environments/environment';
import {ImageCroppedEvent} from 'ngx-image-cropper';
declare  var $: any;
@Component({
  selector: 'app-cover-image',
  templateUrl: './cover-image.component.html',
  styleUrls: ['./cover-image.component.css']
})
export class CoverImageComponent implements OnInit {
  loaderImage = false;
  loaderVideo = false;
  loaderDraft = false;
  loader = true;
  coverVideoEvent;
  coverImage;
  coverVideo;
  classObj = {
    id: '',
    title: '',
    summary: '',
    subject_id: '',
    class_type_id: '',
    from_age: 0,
    to_age: 0,
    size_min: 0,
    size_max: 0,
    class_duration: '',
    class_duration_type: '',
    class_duration_time: '',
    cover_image: '',
    cover_video: '',
    description: '',
    assignment: '',
    goal: '',
    assessment: '',
    hours_per_week: 0,
    parent_guidance: '',
    sources: '',
    teacher_expertise: '',
    external_resources: [],
    class_material_title: '',
    class_material_attachments: [],
    assignment_title: '',
    assignment_attachments: [],
    price_per_student: 0,
    welcome_note: '',
    welcome_cover_path: ''
  };
  savedClassData;
  getSavedClass;
  // locallySavedID;
  assetsUrl = environment.assetsUrl;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  showCropper = false;
  FileType;
  VideoType;
  VideoName;
  id;
  constructor(private apiServices: ApiService,
              private auth: LocalStorageService,
              private snotifyService: SnotifyService,
              private router: Router, private route: ActivatedRoute) {
    $('html, body').animate(
      { scrollTop: '350' }, 800);
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });
    // this.locallySavedID = this.auth.getValue('classID', false);
  }
  ngOnInit(): void {

    if (this.id) {
      this.getClass();
    }
  }
  uploadImage(file) {
    this.loaderImage = true;
    this.croppedImage=file.target.files[0];

    // this.coverPhotoEvent = file.target.files[0];
    // this.coverPhotoEvent = this.croppedImage;
    this.apiServices.uploadCroppedImage(this.croppedImage ).subscribe(res => {
      console.log(res.response);
      this.loaderImage = false;
      this.selectCoverImage(res.response);
      this.classObj.cover_image = res.response;
      $('#cropImageModal').modal('hide');
      this.snotifyService.success('Cover Image successfully uploaded', 'Success');
      
    }, (error) => {
      this.snotifyService.error(error.error.message, 'Error');
      $('#cropImageModal').modal('hide');
      this.loaderImage = false;
    });
  }
  uploadVideoDescription(file) {

    this.loaderVideo = true;
    this.coverVideoEvent = file.target.files[0];
    
    this.apiServices.uploadClassFiles(this.coverVideoEvent).subscribe(res => {
      console.log(res.response);
      this.loaderVideo = false;
      this.selectCoverVideo(res.response);
      this.classObj.cover_video = res.response;
      this.snotifyService.success('Video successfully uploaded', 'Success');
      
    }, (error) => {
      this.snotifyService.error(error.error.message, 'Error');
      this.loaderVideo = false;
    });
    
  }
  getClass() {
    this.loader = true;
    this.apiServices.getAddedClass(this.id).subscribe(res => {
      this.loader = false;
      this.getSavedClass = res.response;
      this.classObj.id = this.getSavedClass.id;
      this.classObj.title = this.getSavedClass.title;
      this.classObj.summary = this.getSavedClass.summary;
      this.classObj.subject_id = this.getSavedClass.subject;
      this.classObj.class_type_id = this.getSavedClass.classType.id;
      this.classObj.from_age = this.getSavedClass.fromAge;
      this.classObj.to_age = this.getSavedClass.toAge;
      this.classObj.size_min = this.getSavedClass.sizeMin;
      this.classObj.size_max = this.getSavedClass.sizeMax;
      this.classObj.class_duration = this.getSavedClass.classDuration;
      this.classObj.class_duration_type = this.getSavedClass.classDurationType;
      this.classObj.class_duration_time = this.getSavedClass.classDurationTime;
      this.classObj.cover_image = this.getSavedClass.coverImage;
      this.classObj.cover_video = this.getSavedClass.coverVideo;
      this.classObj.description = this.getSavedClass.description;
      this.classObj.assignment = this.getSavedClass.assignment;
      this.classObj.goal = this.getSavedClass.goal;
      this.classObj.assessment = this.getSavedClass.assessment;
      this.classObj.hours_per_week = this.getSavedClass.hoursPerWeek;
      this.classObj.parent_guidance = this.getSavedClass.parentGuidance;
      this.classObj.sources = this.getSavedClass.sources;
      this.classObj.teacher_expertise = this.getSavedClass.teacherExpertise;
      this.classObj.external_resources = this.getSavedClass.externalResource;
      if (this.getSavedClass.classMaterial.length > 0) {
        this.classObj.class_material_title = this.getSavedClass.classMaterial[0].information;
      }
      this.classObj.class_material_attachments = this.getSavedClass.classMaterialAttachment;
      if (this.getSavedClass.classAssignment.length > 0) {
        this.classObj.assignment_title = this.getSavedClass.classAssignment[0].information;
      }
      this.classObj.assignment_attachments = this.getSavedClass.classAssingmentAttachment;
      this.classObj.price_per_student = this.getSavedClass.pricePerStudent;
      this.classObj.welcome_note = this.getSavedClass.welcomeNote;
      this.classObj.welcome_cover_path = this.getSavedClass.welcomeCoverImage;

    });
  }

  selectCoverImage(path) {
    this.auth.setValue('cover_image', [path]);
    this.auth.setValue('cover_image_single', path);
  }
  selectCoverVideo(path) {
    this.auth.setValue('cover_video', [path]);
    this.auth.setValue('cover_video_single', path);
  }
  SaveClassDetailsAsDraft() {
    this.loaderDraft = true;
    if (!this.id) {
      this.apiServices.AddClass(this.classObj).subscribe(res => {
        this.loaderDraft = false;
        this.savedClassData = res.response;
        this.snotifyService.success('Saved As Draft Successfully');
        this.classObj.id = this.savedClassData.id;
        this.auth.setValue('classID', this.classObj.id);
      });
    }
    if (this.id) {
      this.apiServices.AddClass(this.classObj).subscribe(res => {
        this.loaderDraft = false;
        this.savedClassData = res.response;
        this.snotifyService.success('Saved As Draft Successfully');
      });
    }
  }
  nextAndSave() {
    if (this.classObj.cover_image && this.classObj.cover_image) {
    this.loader = true;
    if (!this.id) {
      this.apiServices.AddClass(this.classObj).subscribe(res => {
        this.loader = false;
        this.savedClassData = res.response;
        this.classObj.id = this.savedClassData.id;
        this.auth.setValue('classID', this.classObj.id);
        this.router.navigate(['teacher/class/description/' + this.id]);
      });
    }
    if (this.id) {
      this.apiServices.AddClass(this.classObj).subscribe(res => {
        this.loader = false;
        this.savedClassData = res.response;
        this.router.navigate(['teacher/class/description/' + this.id]);
      });
    }
  } else {
      this.snotifyService.warning('Please upload the required fields');
      this.loader = false;
    }
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = this.imageChangedEvent.target.files[0];//event.target.files[0];//.base64;
  }
  fileChangeEvent(event: any) {
    this.FileType = event.target.files[0].type;
    console.log(this.FileType);
    if (this.FileType !== 'image/png' && this.FileType !== 'image/jpeg') {
      this.snotifyService.error('Please select .png or jpg or jpeg');
      $('#cropImageModal').modal('hide');
    } else {
      $('#cropImageModal').modal('show');
      this.imageChangedEvent = event;
    }
  }

  imageLoaded(eve) {
    this.showCropper = true;
    console.log('Image loaded');
  }

  cropperReady(eve) {
  }

  loadImageFailed() {
    console.log('Load failed');
  }

}
