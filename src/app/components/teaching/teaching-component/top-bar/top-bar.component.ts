import { Component, OnInit } from '@angular/core';
import {RouterChangeService} from '../../../../core/services/router-change.service';
import {LocalStorageService} from '../../../../core/services/local-storage.service';
import {environment} from '../../../../../environments/environment';
import {SharedServiceService} from '../../../../core/services/shared-service.service';
import {NavigationEnd, Router, RouterEvent} from '@angular/router';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {
  activeUrl = '/teacher/apply';
  userPresent;
  userFirstName = '';
  userLastName = '';

  constructor(private routerChangeService: RouterChangeService, private router: Router,
              private auth: LocalStorageService, private shared: SharedServiceService) {
    // this.shared.userLoggedIn.subscribe(() => {
    //   this.userPresent = this.auth.getValue(environment.authKey, false);
    // });
    this.routerChangeService.activeRoute.subscribe((e) => {
      this.activeUrl = e.url;
    });
    // this.userPresent = this.auth.getValue(environment.authKey, false);
    /*this.shared.userLoggedIn.subscribe(() => {
      this.userPresent = this.auth.getValue(environment.authKey, false);
    });*/
    // this.userFirstName = this.auth.getValue('firstName', false);
    // this.userLastName = this.auth.getValue('lastName', false);
  }

  ngOnInit(): void {
   /* if (this.userPresent) {
      console.log('USER IS PRESENT');
    } else {
      console.log('USER NOT PRESENT');
    }*/
  }
  logout() {
    this.auth.remove(environment.authKey);
    this.auth.remove('firstName');
    this.auth.remove('lastName');
    this.auth.remove('email');
    this.auth.remove('phone');
    this.auth.remove('username');
    window.location.reload();
  }
}
