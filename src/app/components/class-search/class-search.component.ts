import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { FormControl , FormGroup, FormBuilder} from '@angular/forms';
import { ClassFilterService } from '../../core/services/class-filter.service'
import {ApiService} from '../../core/http/api.service';
import {SnotifyService} from 'ng-snotify';
import { Options } from 'ng5-slider';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import {environment} from '../../../environments/environment.prod';

@Component({
  selector: 'app-class-search',
  templateUrl: './class-search.component.html',
  styleUrls: ['./class-search.component.css']
})
export class ClassSearchComponent implements OnInit {

  data = [];
  Subj: Object[] =[];
  subjects = [];
  isEmpty = false;
  assetsUrl = environment.assetsUrl;

  pricevalue: number = 0;
  pricehighValue: number = 50;
  priceoptions: Options = {
    floor: 0,
    ceil: 50
  };

  agevalue: number = 3;
  agehighValue: number = 25;
  ageoptions: Options = {
    floor: 3,
    ceil: 25
  };

  dropdownSettings:IDropdownSettings;
  countryList = [];
  selectedCountry = [];
  subjectList = [];
  selectedSubject = [];

  filterForm: FormGroup;

  price = {
    used: true,
    val: [this.pricevalue, this.pricehighValue],
    options: this.priceoptions
  };
  age = {
    used: true,
    val: [this.agevalue, this.agehighValue],
    options: this.ageoptions
  };

  sub = {
    used: false,
    selectedSubject: []
  };

  country = {
    used: false,
    selectedCountry: []
  };

  constructor(
                private apiServices: ApiService,
                private router: Router, 
                public pricefilter: ClassFilterService,
                private snotifyService: SnotifyService,
                formbuilder: FormBuilder
                ) { 

    this.filterForm = this.createFormGroup(formbuilder);

    this.getAllSubjects();

    this.pricefilter.getpricefilter(this.price, this.age, this.sub, this.country).subscribe(
      res => {
        console.log(res.response , res.error);
        if(res.response.length===0) 
          this.isEmpty = true;

          console.log(res.message , res.response, "after api call from component");

          res.response.forEach(element => {
            let x: Object = {
              price: element.price_per_student,
              name: element.title,
              body: element.description.substring(0,20),
              cover_image:element.cover_image,
              age_min:element.from_age,
              age_max:element.to_age,
              scheduled_date:element.schedule_date.substring(0,10),
              schedule_start_time: element.schedule_start_time,
              schedule_end_time: element.schedule_end_time,
              max_size:element.size_max,
            }
            this.data.push(x);
          });
      },
      err => { console.log(err); }
    );
  }

  createFormGroup(formBuilder: FormBuilder) {
    return formBuilder.group({
      price: formBuilder.group({
        used: true,
        val: [],
        options: {}
      }),
      age: formBuilder.group({
        used: true,
        val: [],
        options: {}
      }),
      sub: formBuilder.group({
        used: false,
        selectedSubject: []
      }),
      country: formBuilder.group({
        used: false,
        selectedCountry: []
      })
    })
  }

  onSubmit() {
    const result = Object.assign({},this.filterForm.value);
    //console.log(result , "result");
    this.price = Object.assign({},result.price);
    this.age = Object.assign({},result.age);
    this.sub = Object.assign({},result.sub);
    this.country = Object.assign({}, result.country);
    
    this.sub.used = false;
    this.country.used = false;
    
  

    this.pricefilter.getpricefilter(this.price, this.age, this.sub, this.country).subscribe(
      res => {
        console.log(res.response , res.message , "response");
        if(res.response.length===0) 
          this.isEmpty = true;
          this.snotifyService.success(res.message, 'Success');
          this.data=[];
          res.response.forEach(element => {
            let x: Object = {
              price: element.price_per_student,
              name: element.title,
              body: element.description.substring(0,20),
              cover_image:element.cover_image,
              age_min:element.from_age,
              age_max:element.to_age,
              scheduled_date:element.schedule_date.substring(0,10),
              schedule_start_time: element.schedule_start_time,
              schedule_end_time: element.schedule_end_time,
              max_size:element.size_max,
            }
            this.data.push(x);
          });
      },
      (error) => {
        if (error.error.message) {
          this.snotifyService.error(error.error.message, 'Error');
        } else {
          this.snotifyService.error('Please try again later, Server didn\'t respond', 'Error');
        }
      });
  }

  getAllSubjects() {
    let tmp = [];
    this.apiServices.getSubjects()
    .subscribe( res => {
      this.Subj = res.response;
      this.Subj.forEach(element => {
        tmp.push({item_id: element['id'],item_text: element['subject']});
      });
      this.subjectList = tmp;
      //console.log(this.subjectList , "sublist2");
    });

    // this.subjectList = [
    //   {
    //     item_id: 1,
    //     item_text: 'subject1',
    //   },
    //   {
    //     item_id: 2,
    //     item_text: 'subject2',
    //   },
    //   {
    //     item_id: 3,
    //     item_text: 'subject3',
    //   },
    //   {
    //     item_id: 4,
    //     item_text: 'subject4',
    //   },
    // ];
  }

  ngOnInit(): void {
    // this.getAllSubjects();

    this.countryList = [
      { item_id: 1, item_text: 'Canada' },
      { item_id: 2, item_text: 'Singapore' },
      { item_id: 3, item_text: 'India' },
      { item_id: 4, item_text: 'United States' },
      { item_id: 5, item_text: 'Swaziland' }
    ];

    // this.subjectList = [
    //   {
    //     item_id: 1,
    //     item_text: 'subject1',
    //   },
    //   {
    //     item_id: 2,
    //     item_text: 'subject2',
    //   },
    //   {
    //     item_id: 3,
    //     item_text: 'subject3',
    //   },
    //   {
    //     item_id: 4,
    //     item_text: 'subject4',
    //   },
    // ];

    this.selectedCountry = [
      { item_id: 3, item_text: 'Pune' },
      { item_id: 4, item_text: 'Navsari' }
    ];

    // this.selectedSubject = [
    //   { item_id: 1, item_text: 'subject1' },
    //   { item_id: 3, item_text: 'subject3' }
    // ];

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }

}
