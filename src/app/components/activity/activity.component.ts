import { Component, OnInit } from '@angular/core';
import {Router,NavigationEnd} from '@angular/router';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.router.events.subscribe((evt) => {
      window.scrollTo(0, 0)
    });
  }

}
