import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../core/http/api.service';
import {Router,NavigationEnd} from '@angular/router';
import {environment} from '../../../environments/environment.prod';
import {SnotifyService} from 'ng-snotify';
import {LocalStorageService} from '../../core/services/local-storage.service';
import {SocialAuthService, SocialUser} from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import {SharedServiceService} from '../../core/services/shared-service.service';
declare  var $: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginbj = {
    username: '',
    password: '',
  };
  loader;
  user: SocialUser;
  loggedIn: boolean;
  provider;
  userPresent;
  forgetPassword = {
    email: ''
  };
  resetPasswordObj = {
    email: '',
    password: '',
    confirm_password: '',
    otp: ''
  };


  constructor(private apiServices: ApiService,
              private router: Router,
              private snotifyService: SnotifyService,
              private auth: LocalStorageService,
              private authService: SocialAuthService,
              private shared: SharedServiceService) {
  }

  ngOnInit(): void {
    this.router.events.subscribe((evt) => {
      window.scrollTo(0, 0)
    });
    /* this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
      if (this.provider == 'FACEBOOK') {
        this.validateFb();
      }
      if (this.provider == 'GOOGLE') {
        this.validateGoogle();
      }
    });*/
  }
  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.provider = GoogleLoginProvider.PROVIDER_ID;
    console.log(this.provider);
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    this.provider = FacebookLoginProvider.PROVIDER_ID;
    console.log(this.provider);
  }

  Login() {
    this.loader = true;
    this.apiServices.Login(this.loginbj).subscribe(res => {
      this.loader = false;
      this.auth.setValue(environment.authKey, res.token);
      this.auth.setValue('firstName', res.user[0].firstName);
      this.auth.setValue('lastName', res.user[0].lastName);
      this.auth.setValue('email', res.user[0].email);
      this.auth.setValue('phone', res.user[0].phone);
      this.auth.setValue('username', res.user[0].username);
      this.userPresent = !!this.auth.getValue(environment.authKey, false);
      this.auth.remove('classID');
      this.shared.userLoggedIn.emit();
      
      if(res.user[0].role!=='Learner')
        this.router.navigate(['/teacher/apply']);
      else
      this.router.navigate(['/']);  
    }, (error) => {
      this.loader = false;
      this.snotifyService.error(error.error.message, 'Error');
    });
  }
  validateFb() {
    this.apiServices.validateFb({fb_id: this.user.id}).subscribe( res => {
      console.log(res.response);
      if (res.response) {
        this.router.navigate(['/teacher/apply']);
      }
    });
  }
  validateGoogle() {
    this.apiServices.validateGoogle({google_id: this.user.id}).subscribe( res => {
      console.log(res.response);
      if (res.response) {
        this.router.navigate(['/teacher/apply']);
      }
    });
  }
  forgotPassword() {
    this.apiServices.forgetPassword(this.forgetPassword).subscribe( res => {
      this.resetPasswordObj.email = this.forgetPassword.email;
      this.snotifyService.success(res.message, 'Success');
      $('#forgetPassword').modal('hide');
      $('#resetPassword').modal('show');
    }, (error) => {
      this.snotifyService.error(error.error.message, 'Error');
    });
  }
  resetPassword() {
    this.apiServices.resetPassword(this.resetPasswordObj).subscribe( res => {
      this.snotifyService.success(res.message, 'Success');
      $('#resetPassword').modal('hide');
    }, (error) => {
      this.snotifyService.error(error.error.message, 'Error');
    });
  }
}
