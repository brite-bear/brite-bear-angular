import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearnerCompleteSignupComponent } from './learner-complete-signup.component';

describe('LearnerCompleteSignupComponent', () => {
  let component: LearnerCompleteSignupComponent;
  let fixture: ComponentFixture<LearnerCompleteSignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearnerCompleteSignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearnerCompleteSignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
