import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../core/http/api.service';
import {environment} from '../../../environments/environment';
import {LocalStorageService} from '../../core/services/local-storage.service';
import {Router} from '@angular/router';
import {SnotifyService} from 'ng-snotify';
import {FacebookLoginProvider, GoogleLoginProvider, SocialAuthService, SocialUser} from 'angularx-social-login';
import {SharedServiceService} from '../../core/services/shared-service.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signUpObj = {
    username: '',
    email: '',
    password: '',
    confirm_password: '',
    role:''
  };
  user: SocialUser;
  userPresent;
  loggedIn: boolean;
  selectedRole: any;

  constructor(private apiServices: ApiService,
              private auth: LocalStorageService,
              private router: Router, private shared: SharedServiceService,
              private snotifyService: SnotifyService,
              private authService: SocialAuthService) {
  }

  ngOnInit(): void {
    this.signUpObj.role="teacher";
    
    this.authService.authState.subscribe((user) => {
      
      this.user = user;
      console.log(this.user.email);
      console.log(this.user.name);
      console.log(this.user.firstName);
      console.log(this.user.lastName);
      console.log(this.user.photoUrl);
      console.log(this.user.id);
      this.loggedIn = (user != null);
      this.validateFb();
      this.validateGoogle();
    });
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    console.log(this.authService);
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  
  signUp() {
    this.apiServices.signUp(this.signUpObj).subscribe(res => {
      this.auth.setValue(environment.authKey, res.token);
      this.auth.setValue('firstName', res.response.firstName);
      this.auth.setValue('lastName', res.response.lastName);
      this.auth.setValue('email', res.response.email);
      this.auth.setValue('phone', res.response.phone);
      this.auth.setValue('username', res.response.username);
      this.auth.remove('classID');
      this.userPresent = !!this.auth.getValue(environment.authKey, false);
      this.snotifyService.success('Registered Successfully, You are Almost Done', 'Success');
      this.router.navigate(['../complete-signUp']);
      
      // this.router.navigate(['/teacher/apply']);
      
    }, (error) => {
      if (error.error.message) {
        this.snotifyService.error(error.error.message, 'Error');
      } else {
        this.snotifyService.error('Please try again later, Server didn\'t respond', 'Error');
      }
    });
  }

  validateFb() {
    this.apiServices.validateFb({fb_id: this.user.id}).subscribe(res => {
      console.log(res.response);
      if (res.response) {
        this.router.navigate(['../complete-signUp']);
      }
    });
  }

  validateGoogle() {
    this.apiServices.validateGoogle({google_id: this.user.id}).subscribe(res => {
      console.log(res.response);
      if (res.response) {
        this.router.navigate(['../complete-signUp']);
      }
    });
  }

}
