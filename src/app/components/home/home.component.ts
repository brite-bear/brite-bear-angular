import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {RouterChangeService} from '../../core/services/router-change.service';
import {environment} from '../../../environments/environment.prod';
import {LocalStorageService} from '../../core/services/local-storage.service';
import {ApiService} from '../../core/http/api.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  activeUrl;
  isActive=true;
  assetsUrl = environment.assetsUrl;
  userPresent;
  classObj: any = {
    data:[]	
  } 


  slides = [
    {img: './assets/images/img3.png'},
    {img: './assets/images/img3.png'},
    {img: './assets/images/img3.png'},
    {img: './assets/images/img3.png'},
    {img: './assets/images/img3.png'},
    {img: './assets/images/img3.png'},
    {img: './assets/images/img3.png'}
  ];
  slidesParents = [
    {img: './assets/images/video.png'},
    {img: './assets/images/video.png'},
    {img: './assets/images/video.png'},
    {img: './assets/images/video.png'},
  ];
  slideConfig = {slidesToShow: 5, slidesToScroll: 1};
  slideParentConfig = {slidesToShow: 2, slidesToScroll: 1};
  constructor(private router: Router, private auth: LocalStorageService,
              private routerChangeService: RouterChangeService, private apiServices: ApiService) {
    this.routerChangeService.activeRoute.subscribe((e) => {
      this.activeUrl = e.url;
    });
    this.userPresent = !!this.auth.getValue(environment.authKey, false);
    if (this.userPresent) {
      this.router.navigate(['/']);
    }
  }
  ngOnInit(): void {
    this.getClass();
  }
  
  aboutUs() {
    // this.router.navigate(['teacher/apply']);
    window.open('about');
  }
  notify(){
    
  }

  async getClass() {
    this.apiServices.getClassList().subscribe(res => {
      this.classObj.data  = res.response;
      
      this.classObj.data.forEach(element => {
        element.price_per_student=Math.floor(Math.floor(element.price_per_student*73.14)/100)*100+99;
        if(element.title.length>18)
        {
          element.title=element.title.substring(0,16);
          element.title=element.title+'...';  
        }
      });
      
      
    })
  }

  previewClass(id){
    
    this.router.navigate(['/preview/'+id]);  
    
  } 

  classsearch() {
    this.router.navigate(['/class-search']);
  }
  addSlide() {
    this.slides.push({img: 'http://placehold.it/350x150/777777'});
  }

  removeSlide() {
    this.slides.length = this.slides.length - 1;
  }

  slickInit(e) {
    // console.log('slick initialized');
  }

  breakpoint(e) {
    // console.log('breakpoint');
  }

  afterChange(e) {
    // console.log('afterChange');
  }

  beforeChange(e) {
    // console.log('beforeChange');
  }
 /* startFromHtml() {
    this.router.navigate([''])
  }*/

}
