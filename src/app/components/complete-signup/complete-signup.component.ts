import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../core/http/api.service';
import {LocalStorageService} from '../../core/services/local-storage.service';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment.prod';
import {SnotifyService} from 'ng-snotify';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {SharedServiceService} from '../../core/services/shared-service.service';

declare var $: any;

@Component({
  selector: 'app-complete-signup',
  templateUrl: './complete-signup.component.html',
  styleUrls: ['./complete-signup.component.css']
})
export class CompleteSignupComponent implements OnInit {
  completeSignUpObj = {
    first_name: '',
    last_name: '',
    phone: '',
    phone_id:'',
    country_id: '',
    region_id: '',
    city_id: '',
    profile_image: '',
    referral_code: ''
  };
  signUpData;
  allCountries = [];
  allStates = [];
  allCities = [];
  loaderImage = false;
  profileChangedEvent;
  profileImage;
  assetsUrl = environment.assetsUrl;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  showCropper = false;
  image;
  FileType;
  userPresent;

  constructor(private apiServices: ApiService,
              private auth: LocalStorageService, private route: ActivatedRoute,
              private router: Router, private shared: SharedServiceService,
              private snotifyService: SnotifyService) {
    this.auth.getValue('auth_token', false);
  }

  ngOnInit(): void {
    this.getCountries();
  }

  completeSignUp() {
    if(this.completeSignUpObj.phone_id=='')
      this.completeSignUpObj.phone_id='+91'
    
    this.completeSignUpObj.phone=this.completeSignUpObj.phone_id+this.completeSignUpObj.phone;
    
    this.completeSignUpObj.profile_image = this.profileImage;
    this.apiServices.completeSignUp(this.completeSignUpObj).subscribe(res => {
      this.signUpData = res.response;
      this.auth.setValue('signUpDetails', this.signUpData);
      this.auth.setValue('firstName', res.response[0].firstName);
      this.auth.setValue('lastName', res.response[0].lastName);
      this.auth.setValue('email', res.response[0].email);
      this.auth.setValue('phone', res.response[0].phone);
      this.auth.setValue('username', res.response[0].username);
      this.userPresent = !!this.auth.getValue(environment.authKey, false);
      this.snotifyService.success('Congratulation, Your Profile is Completed', 'Success');
      this.shared.userLoggedIn.emit();
      this.router.navigate(['/teacher/apply']);
      // this.userPresent = !!this.auth.getValue(environment.authKey, false);
    }, (error) => {
      if (error.error.message) {
        this.snotifyService.error(error.error.message, 'Error');
      } else {
        this.snotifyService.error('Please try again later, Server didn\'t respond', 'Error');
      }
    });
  }

  getCountries() {
    this.apiServices.getCountries().subscribe(res => {
      this.allCountries = res.response;
    });
  }

  getStates() {
    this.apiServices.getStates(this.completeSignUpObj.country_id).subscribe(res => {
      this.allStates = res.response;
      this.allStates.push({name:'Not in the List',id:-1});
    });
  }

  getCities() {
    this.apiServices.getCities(this.completeSignUpObj.region_id).subscribe(res => {
      this.allCities = res.response;
      this.allCities.push({name:'Not in the List',id:-1});
    });
  }

  uploadProfileImage(file) {
    
    this.loaderImage = true;
    this.croppedImage=file.target.files[0];
    this.apiServices.uploadTeacherProfile(this.croppedImage).subscribe(res => {
      console.log(res.response);
      this.selectImage(res.response);
      this.profileImage = res.response;
      console.log(this.assetsUrl + this.profileImage);
      $('#cropImageModal').modal('hide');
      this.snotifyService.success('Image successfully uploaded', 'Success');
      this.loaderImage = false;
    }, (error) => {
      this.snotifyService.error(error.error.message, 'Error');
      $('#cropImageModal').modal('hide');
      this.loaderImage = false;
    });
  }

  selectImage(path) {
    this.auth.setValue('profile_image', [path]);
    this.auth.setValue('profile_image_single', path);
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    console.log(this.croppedImage);
  }

  /* fileChangeEvent(event: any): void {
     $('#cropImageModal').modal('show');
     this.imageChangedEvent = event;
   }*/
  fileChangeEvent(event: any) {
    this.FileType = event.target.files[0].type;
    console.log(this.FileType);
    if (this.FileType !== 'image/png' && this.FileType !== 'image/jpeg') {
      this.snotifyService.error('Please select .png or jpg or jpeg');
      $('#cropImageModal').modal('hide');
    } else {
      $('#cropImageModal').modal('show');
      this.imageChangedEvent = event;
    }
  }

  imageLoaded() {
    this.showCropper = true;
    console.log('Image loaded');
  }

  cropperReady() {
  }

  loadImageFailed() {
    console.log('Load failed');
  }
}
