import { Component, OnInit } from '@angular/core';
import {RouterChangeService} from '../../core/services/router-change.service';
import {LocalStorageService} from '../../core/services/local-storage.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  activeUrl;
  constructor(private routerChangeService: RouterChangeService,
              private auth: LocalStorageService) {
    this.routerChangeService.activeRoute.subscribe((e) => {
      this.activeUrl = e.url;
    });
  }

  ngOnInit(): void {
  }
  contactUs() {
    // this.router.navigate(['teacher/apply']);
    window.open('contact');
  }
  aboutUs() {
    // this.router.navigate(['teacher/apply']);
    window.open('about');
  }
  vision() {
    window.open('vision');
  }
  faq() {
    // this.router.navigate(['teacher/apply']);
    window.open('faq');
  }
  blog() {
    window.open('blog');
  }
  news() {
    window.open('news');
  }
  ourTeachers() {
    window.open('our-teachers');
  }
  gallery() {
    window.open('gallery');
  }
  activity() {
    window.open('activity');
  }

}
