import {Component, OnInit,SimpleChanges} from '@angular/core';
import {SharedServiceService} from '../../core/services/shared-service.service';
import {environment} from '../../../environments/environment.prod';
import {LocalStorageService} from '../../core/services/local-storage.service';
import {RouterChangeService} from '../../core/services/router-change.service';
import {Router} from '@angular/router';
import {ApiService} from '../../core/http/api.service';
import { getDiffieHellman } from 'crypto';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userPresent;
  activeUrl = '/teacher/apply';
  userFirstName = '';
  userLastName = '';
  role='';
  id='';

  constructor(private apiServices: ApiService,private shared: SharedServiceService, private router: Router,
              private auth: LocalStorageService, private routerChangeService: RouterChangeService) {
    this.userPresent = this.auth.getValue(environment.authKey, false);
    this.shared.userLoggedIn.subscribe(() => {
      this.userPresent = this.auth.getValue(environment.authKey, false);
      //console.log("Hi",this.userPresent)
      if (this.userPresent) {
        this.userFirstName = this.auth.getValue('firstName', false);
        this.userLastName = this.auth.getValue('lastName', false);
        
      }
      this.userFirstName = this.auth.getValue('firstName', false);
      this.userLastName = this.auth.getValue('lastName', false);
      
    });
    /*this.userFirstName = this.auth.getValue('firstName', false);
    this.userLastName = this.auth.getValue('lastName', false);
    this.routerChangeService.activeRoute.subscribe((e) => {
      this.activeUrl = e.url;
      console.log(this.activeUrl);
      this.userPresent = this.auth.getValue(environment.authKey, false);
      /!*this.userFirstName = this.auth.getValue('firstName', false);
      this.userLastName = this.auth.getValue('lastName', false);*!/
    });*/
    if (this.userPresent) {
      this.userPresent = this.auth.getValue(environment.authKey, false);
    }
    this.userFirstName = this.auth.getValue('firstName', false);
    this.userLastName = this.auth.getValue('lastName', false);
    
  }

  ngOnInit(): void {
    this.userPresent = this.auth.getValue(environment.authKey, false);
    
    this.userFirstName = this.auth.getValue('firstName', false);
    
    this.userLastName = this.auth.getValue('lastName', false);
    //this.getrole();
  }

  ngOnChanges(changes:SimpleChanges ) {
    console.log("Yes it is changing");
    this.getrole();
  }
  
  async getrole(){  
    this.apiServices.getrole().subscribe(res => {
      this.role=res.response;
      
    })}
  
  async getid(){
    this.apiServices.getid().subscribe(res => {
      
      this.id=res.response;
      window.open('others-profile/'+this.id);
      
    })
    
  }  
    
  logout() {
    this.auth.remove(environment.authKey);
    this.auth.remove('firstName');
    this.auth.remove('lastName');
    this.auth.remove('email');
    this.auth.remove('phone');
    this.auth.remove('username');
    window.location.reload();
  }

  profile() {
    window.open('profile');
    // this.router.navigate(['profile']);
  }

  

  Dashboard() {
    // this.router.navigate(['teacher/apply']);
    window.open('teacher/apply');
  }

  Settings() {
    window.open('settings');
  }

}
