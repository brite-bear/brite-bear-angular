import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../core/http/api.service';
import {Router,NavigationEnd} from '@angular/router';
import {environment} from '../../../environments/environment.prod';
import {SnotifyService} from 'ng-snotify';
import {LocalStorageService} from '../../core/services/local-storage.service';
import {SocialAuthService, SocialUser} from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import {SharedServiceService} from '../../core/services/shared-service.service';
declare  var $: any;
@Component({
  selector: 'app-free-class',
  templateUrl: './free-class.component.html',
  styleUrls: ['./free-class.component.css']
})
export class FreeClassComponent implements OnInit {
  detailsbj = {
    childname:'',
    name: '',
    email: '',
    phone:'',
    DOB:'',
    subject:'',
    note:'',
    phone_id:''
  };
  allCountries=[];
  loader;
  user: SocialUser;
  provider;
  userPresent;
  

  constructor(private apiServices: ApiService,
              private router: Router,
              private snotifyService: SnotifyService,
              private auth: LocalStorageService,
              private authService: SocialAuthService,
              private shared: SharedServiceService) {
  }

  ngOnInit(): void {
      this.router.events.subscribe((evt) => {
        window.scrollTo(0, 0)
      });
      this.getCountries();
  }
  
  SendDetails() {
    this.loader = true;
    if(this.detailsbj.phone_id=='')
      this.detailsbj.phone_id='+91'
    
      this.detailsbj.phone=this.detailsbj.phone_id+this.detailsbj.phone;
    
    this.apiServices.FreeClassdetails(this.detailsbj).subscribe(res => {
      this.loader = false;
      this.snotifyService.success('Thanks for providing Information.We will let you know when your favourite class will be available', 'Success');
      this.router.navigate(['/']);
    }, (error) => {
      this.loader = false;
      this.snotifyService.error(error.error.message, 'Error');
    });
  }
  
  getCountries() {
    this.apiServices.getCountries().subscribe(res => {
      this.allCountries = res.response;
    });
  }
  
}
