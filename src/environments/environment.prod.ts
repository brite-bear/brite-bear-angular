export const environment = {
  production: true,
  authKey: 'brite_token',
  apiPrefix: 'https://api.britebear.com/api/v1/',
  // apiPrefix: 'http://localhost/britebear/api/',
  // assetsUrl: 'http://159.65.142.31:1338/public/storage',
  assetsUrl: 'https://britebear-s3-pp.s3-ap-southeast-1.amazonaws.com/',
};
