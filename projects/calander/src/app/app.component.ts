import { Component, ViewChild } from '@angular/core';
//import { scheduleClassData } from './data';
import { extend } from '@syncfusion/ej2-base';
import {
     ScheduleComponent, EventSettingsModel, View, EventRenderedArgs, AgendaService, DayService, WeekService, MonthService, ResizeService, DragAndDropService, PopupOpenEventArgs, ResizeEventArgs 
} from '@syncfusion/ej2-angular-schedule';
import { ScheduleClassService } from './services/schedule-class.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DayService, WeekService, MonthService, AgendaService, ResizeService, DragAndDropService]
})
export class AppComponent {
  title = 'calander';
  public scheduleClassData : Object[];
  public eventSettings: EventSettingsModel ;

   constructor( private scheduleclassservice : ScheduleClassService) {
        
        this.scheduleclassservice.scheduledClasses()
        .then( res => { 
            console.log(res["response"]);
            let arr: Object[] = [];
            this.scheduleClassData = res["response"];
            this.scheduleClassData.forEach((ele) => {
                let y = ele["schedule_date"].substring(0,4);
                let m = ele["schedule_date"].substring(5,7);
                let dt = ele["schedule_date"].substring(8,10);
                let hr1 = ele["schedule_start_time"].substring(0,2);
                let min1 = ele["schedule_start_time"].substring(3,5);
                let hr2 = ele["schedule_end_time"].substring(0,2);
                let min2 = ele["schedule_end_time"].substring(3,5);

                arr.push({
                    Id: ele["id"],
                    Subject: ele["title"],
                    StartTime: new Date(y,m-1,dt,hr1,min1),
                    EndTime: new Date(y,m-1,dt,hr2,min2),
                    IsBooked: false,
                    CategoryColor: '#f57f17'
                })
            });

            this.eventSettings = { dataSource: <Object[]>extend([], arr, null, true) }; 
        });

        //this.eventSettings = { dataSource: <Object[]>extend([], this.scheduleClassData, null, true) };

   }

   @ViewChild('scheduleObj')
    public scheduleObj: ScheduleComponent;
    public AllowDragAndDrop = false;
    public AllowDelete = false;
    public selectedDate: Date = new Date;
    
    public scheduleView: View = 'Week';
    public datas: string[] = ['Day', 'Week', 'Month', 'Agenda'];

    oneventRendered(args: EventRenderedArgs): void {
        let categoryColor: string = args.data.CategoryColor as string;
        if (!args.element || !categoryColor) {
            return;
        }
        if (this.scheduleView === 'Agenda') {
            (args.element.firstChild as HTMLElement).style.borderLeftColor = categoryColor;
        } else {
            args.element.style.backgroundColor = categoryColor;
        }
    }

    onDataBound(): void {
        let event: Object[] = this.scheduleObj.getEvents();
        console.log(event);
    }

    onPopupOpen(args: PopupOpenEventArgs): void {
        if ( args.type === 'QuickInfo' )  {
            args.cancel = true;
        }
    }

    onResizeStop(args: ResizeEventArgs): void {
        //args.cancel = this.onEventCheck(args);
        args.cancel = true;
    }
}
