import { Injectable } from '@angular/core';
// import { scheduleClassData } from '../data';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ScheduleClassService {

  data: Object = {};
  scheduleData: Object[];

  constructor(private httpClient: HttpClient) { }

  // getData() {
  //   scheduleClassData.forEach((ele)=>{if(ele["IsBooked"]==true) ele["CategoryColor"]="#1aaa55"; else ele["CategoryColor"]="#f57f17";})
  //   return scheduleClassData;
  // }

  async scheduledClasses(): Promise<Observable<any>>{
    //return this.httpClient.get('class/list');

        try {
                let response = await this.httpClient.get('class/list')
                .toPromise();

              return response as Observable<any>;

          } catch (error) {
            console.log(error);
          }
    }

}
